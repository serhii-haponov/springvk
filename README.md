# Authorization #
![Screenshot 2017-04-24 15.16.14.png](https://bitbucket.org/repo/Gxy9jb/images/3648885325-Screenshot%202017-04-24%2015.16.14.png)

# Interface for different Size Classes; Side menu #
![Screenshot 2017-04-24 15.16.38.png](https://bitbucket.org/repo/Gxy9jb/images/4132645222-Screenshot%202017-04-24%2015.16.38.png)
![Screenshot 2017-04-24 15.20.05.png](https://bitbucket.org/repo/Gxy9jb/images/406941922-Screenshot%202017-04-24%2015.20.05.png)
![Screenshot 2017-04-24 15.16.52.png](https://bitbucket.org/repo/Gxy9jb/images/3044222974-Screenshot%202017-04-24%2015.16.52.png)

# Review list of Friends (or Followers) and quick search them
![Screenshot 2017-04-24 15.17.35.png](https://bitbucket.org/repo/Gxy9jb/images/3066201436-Screenshot%202017-04-24%2015.17.35.png)
![Screenshot 2017-04-24 15.25.15.png](https://bitbucket.org/repo/Gxy9jb/images/2580671538-Screenshot%202017-04-24%2015.25.15.png)

# Review wall and comments chosen person or group#
![Screenshot 2017-04-24 15.22.06.png](https://bitbucket.org/repo/Gxy9jb/images/3341172787-Screenshot%202017-04-24%2015.22.06.png)
![Screenshot 2017-04-24 15.22.14.png](https://bitbucket.org/repo/Gxy9jb/images/2703582765-Screenshot%202017-04-24%2015.22.14.png)

# Post text any length #
![Screenshot 2017-04-24 16.27.43.png](https://bitbucket.org/repo/Gxy9jb/images/515278308-Screenshot%202017-04-24%2016.27.43.png)

** Pull up to refresh; Plus button uses for quick making post from the any place wherever the scroll located.**