//
//  SGFriendCell.h
//  45_APITest
//
//  Created by Sergey on 1/9/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SGUser;

@interface SGFriendCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *onlineBadge;

-(void)setupWithUser:(SGUser *)user;

@end
