//
//  SGAudioObject.m
//  45_APITest
//
//  Created by Sergey on 1/20/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGAudioObject.h"

@implementation SGAudioObject

- (id)initWithServerResponse:(NSDictionary *)responseObject {
   self = [super init];
    
    if (self) {
        
        self.audioID = [responseObject objectForKey:@"aid"];
        self.artist = [responseObject objectForKey:@"artist"];
        self.duration = [responseObject objectForKey:@"adurationid"];
        self.genre = [responseObject objectForKey:@"genre"];
        self.owner_ID = [responseObject objectForKey:@"owner_id"];
        self.performer = [responseObject objectForKey:@"performer"];
        self.title = [responseObject objectForKey:@"title"];
        self.url = [responseObject objectForKey:@"url"];
    }
    return self;
}



@end
