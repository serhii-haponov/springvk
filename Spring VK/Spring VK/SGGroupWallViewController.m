//
//  SGGroupWallViewController.m
//  45_APITest
//
//  Created by Sergey on 12/27/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "SGGroupWallViewController.h"
#import "SGCommetnsVC.h"

#import "SGServerManager.h"
#import "SGPostCell.h"
#import "SGAttachment.h"

#import "SGPost.h"
#import "SGUniversalWallCell.h"

#import "SGLikeRepost.h"

@interface SGGroupWallViewController () <SGPostCellDelegate, SGLikeRepostDelegate>

@property (strong, nonatomic) NSMutableArray *postsArray;
@property (assign, nonatomic) CGFloat heightRow;
@property (strong, nonatomic) NSURLSessionDataTask *currentTask;
@property (strong, nonatomic) SGPostCell *postFirstCell;
@property (weak, nonatomic)   SGCommetnsVC *commentVC;

@end

@implementation SGGroupWallViewController

static NSInteger wallTextInRequest = 5;
static const NSInteger loadPostCell = 1;
static const NSInteger postTextCell = 1;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:0.90 green:0.90 blue:0.90 alpha:1];
    
    self.tableView.estimatedRowHeight = 60;

    [self.tableView registerNib:[UINib nibWithNibName:@"SGPhotoCell" bundle:nil] forCellReuseIdentifier:@"photoCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SGPostCell" bundle:nil] forCellReuseIdentifier:@"TextFieldCell"];

    self.postsArray = [[NSMutableArray alloc] init];

     self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    
    [refresh addTarget:self action:@selector(wallRefresh) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.refreshControl = refresh;
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(actionPost)];
    self.navigationItem.rightBarButtonItem = barButton;
    
    [self getWallTextFromServer];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self wallRefresh];
}

#pragma mark - Orientation

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {

    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {

        [self.tableView reloadData];
    }];
}

#pragma mark - API methods

- (void)getWallTextFromServer {

    self.currentTask = [[SGServerManager sharedManager] getGroupWall:self.currentID withOffSet:[self.postsArray count] andCount:wallTextInRequest onSuccess:^(NSArray *post) {
      
        [self.postsArray addObjectsFromArray:post];
        [self.tableView reloadData];
    
    } onFailure:^(NSError *error, NSInteger statuseCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statuseCode);
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.postsArray count] != [SGServerManager sharedManager].quantityPosts){
        return [self.postsArray count] + postTextCell + loadPostCell;
    } else {
        return [self.postsArray count] + postTextCell;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        static NSString *identifierCell = @"TextFieldCell";
        
        SGPostCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierCell];
        self.postFirstCell = cell;
        cell.delegate = self;
        return cell;
        
    } else if (indexPath.row < [self.postsArray count] + postTextCell) {
 
        static NSString *identifierCell = @"UniversalCell";
        
        SGUniversalWallCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierCell];
        
        if (!cell) {
            
            cell = [[SGUniversalWallCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifierCell];
        }
        
        SGPost *post = [self.postsArray objectAtIndex:indexPath.row - postTextCell];

        cell.cellPost = post;
        
        cell.likeRepostView.commentButton.tag = indexPath.row - postTextCell;
        cell.likeRepostView.delegate = self;
    
        if (indexPath.row == [self.postsArray count] - 1 && indexPath.row != [SGServerManager sharedManager].quantityPosts - 1) {
            
            [self.currentTask cancel];
            [self getWallTextFromServer];
        }
        
        return cell;
        
    } else {
        
        static NSString* identifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        cell.textLabel.text = @"Press for \"LOAD\"";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.contentView.backgroundColor = [UIColor yellowColor];
        
        return cell;
    }
    return nil;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [self.postsArray count]) {
        
        [self.currentTask cancel];
        [self getWallTextFromServer];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
    
        return UITableViewAutomaticDimension;
        
    } else
        if (indexPath.row < [self.postsArray count] + postTextCell) {
        
        SGPost *post = [self.postsArray objectAtIndex:indexPath.row - postTextCell];
        
        SGUniversalWallCell *cell = [[SGUniversalWallCell alloc] init];
        
        return [cell heightForRow:post];
        
    } else {
        return [tableView rowHeight];
    }
    
    return [tableView rowHeight];
}

#pragma mark - Action method

- (void)actionPost {
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.postFirstCell.postTextFieldView becomeFirstResponder];
    });
}

- (void)wallRefresh {
    
    [[SGServerManager sharedManager] getGroupWall:self.currentID withOffSet:0 andCount:MAX(wallTextInRequest, [self.postsArray count])  onSuccess:^(NSArray *post) {
        
        [self.postsArray removeAllObjects];
        [self.postsArray addObjectsFromArray:post];
        
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
        
    } onFailure:^(NSError *error, NSInteger statuseCode) {
        
    }];
}

#pragma mark - SGPostCellDelegate

- (void)postCell:(SGPostCell *)cell {
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

- (void)postCell:(SGPostCell *)cell postText:(NSString *)text {
    
    [[SGServerManager sharedManager] postText:text forGroup:self.currentID inSuccess:^(id result) {
        
        [self wallRefresh];
    } inFailure:^(NSError *error, NSInteger statusCode) {
    }];
}

#pragma mark - setLikeRepVC

- (SGCommetnsVC *)commentVC {
    
    if (!_commentVC) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _commentVC = [sb instantiateViewControllerWithIdentifier:@"sg_comments"];
    }
    return _commentVC;
}

#pragma mark - SGLikeRepostDelegate

- (void)likeRepostComment:(SGLikeRepost *)comment tagButton:(NSInteger)tagB {
    
    SGPost *post = [self.postsArray objectAtIndex:tagB];

    self.commentVC.ownerCommtentsID = [post.toID stringValue];
    self.commentVC.postCommtentsID = [post.itemID stringValue];
    self.commentVC.quantityComments = [post.postCountComents unsignedIntegerValue];
    
    [self.navigationController pushViewController:self.commentVC animated:YES];
}

@end
