//
//  SGGroup.h
//  45_APITest
//
//  Created by Sergey on 1/14/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGServerObject.h"
#import "SGSubscriptionProtocol.h"

@interface SGCommunity : SGServerObject <SGSubscriptionProtocol>

@property (nonatomic, strong) NSNumber *gID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *photoUrl;

@end
