//
//  SGCommetnsVC.m
//  Spring VK
//
//  Created by Sergey on 2/27/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGCommetnsVC.h"
#import "SGGroupWallViewController.h"

#import "SGServerManager.h"

#import "SGCommantCell.h"
#import "SGPostCell.h"

#import "SGLikeRepost.h"

@interface SGCommetnsVC () <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>

@property (strong, nonatomic) NSMutableArray *arrayComments;
@property (strong, nonatomic) NSURLSessionDataTask *currentTask;

@end

@implementation SGCommetnsVC 

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setup];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillAppeared:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillDisappeared:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SGPostCell" bundle:nil] forCellReuseIdentifier:@"TextFieldCell"];
    
    self.commentTextView.layer.masksToBounds = YES;
    self.commentTextView.layer.cornerRadius = 3.f;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 130;
    
    self.arrayComments = [[NSMutableArray alloc] init];
    
    self.tableView.allowsSelection = NO;
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self getCommentsFromServerManager:nil];
}

#pragma mark - SGServerManager method

- (void)getCommentsFromServerManager:(void (^)(void))complition {
    
   self.currentTask = [[SGServerManager sharedManager] getOwnersWallComments:self.ownerCommtentsID ownerPost:self.postCommtentsID withCount:self.quantityComments andOffset:[self.arrayComments count] inSuccess:^(NSArray *comments) {
        
        [self.arrayComments addObjectsFromArray:comments];
        
        for(SGComment *commentObj in comments) {
            [[SGServerManager sharedManager] getUser:[commentObj.comUserID stringValue] onSuccess:^(SGUser *user) {
                
                [commentObj getUserData:user];
                [self.tableView reloadData];
                
                if (complition && [[comments lastObject] isEqual:commentObj]) {
                    complition();
                }
            } onFailure:^(NSError *error, NSInteger statuseCode) {
                NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statuseCode);
            }];
        }
    } inFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.arrayComments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SGCommantCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commetnCell" forIndexPath:indexPath];
    
    SGComment *commentObj = [self.arrayComments objectAtIndex:indexPath.row];
    
    cell.commentSetObjs = commentObj;

    return cell;
}

#pragma mark - NSNotificationCenter

- (void)keyBoardWillAppeared:(NSNotification *)notification {
    
    NSValue *valueKeyboard = [notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect rectKeyboard = [valueKeyboard CGRectValue];
    CGFloat heightKeyboard = CGRectGetHeight(rectKeyboard);
    self.bottomConstraintTextView.constant = heightKeyboard;
  
    [UIView animateWithDuration:2.5 delay:0 options:7 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
    }];
    
    if (self.quantityComments != 0) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:self.quantityComments - 1 inSection:0];
        [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}

- (void)keyBoardWillDisappeared:(NSNotification *)notification {
   
    self.bottomConstraintTextView.constant = 0;
    
    [UIView animateWithDuration:2.5 delay:0 options:7 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:@"Leave your comment here"]) {
        textView.text = @"";
        textView.textColor = [UIColor whiteColor];
    }
}

#pragma mark - commentTextView.inputAccessoryView

-(void)setup {
    
    CGRect bounds = [UIScreen mainScreen].bounds;
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, bounds.size.width, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle: @"Send" style: UIBarButtonItemStylePlain target: self action: @selector(doneKeyboardClick)];
    [doneButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:20], NSForegroundColorAttributeName : [UIColor purpleColor]} forState: UIControlStateNormal];
    toolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton];
    [toolbar sizeToFit];
    self.commentTextView.inputAccessoryView = toolbar;
}

-(void)doneKeyboardClick{
    
    if ([self.commentTextView.text length] != 0) {
        
        [[SGServerManager sharedManager] createComment:self.commentTextView.text inWallID:self.ownerCommtentsID forPostID:self.postCommtentsID inSuccess:^(id result) {
            
            [self.currentTask cancel];
            [self getCommentsFromServerManager:^{
                
                if (self.quantityComments != 0) {
                    NSIndexPath *path = [NSIndexPath indexPathForRow:self.quantityComments - 1 inSection:0];
                    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:NO];
                }
            }];
        } inFailure:^(NSError *error, NSInteger statusCode) {
            
        }];
    }
    self.commentTextView.text = @"Leave your comment here";
    self.commentTextView.textColor = [UIColor colorWithWhite:1.f alpha:0.7];
    [self.commentTextView resignFirstResponder];
}

@end
