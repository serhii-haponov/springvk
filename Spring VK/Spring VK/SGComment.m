//
//  SGComment.m
//  Spring VK
//
//  Created by Sergey on 2/23/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGComment.h"

@implementation SGComment

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    self = [super init];
    
    if (self) {
        
        self.canEdit = [responseObject objectForKey:@"can_edit"];
        self.comUserID = [responseObject objectForKey:@"uid"];
        self.commentID = [responseObject objectForKey:@"cid"];
        self.commentDate = [self createDateFromSec:[responseObject objectForKey:@"date"]];
        
        NSString *textString = [responseObject objectForKey:@"text"];
        self.commentText = [textString stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    }
    return self;
}

- (void)getUserData:(SGUser *)user {
    
    self.firstName = user.firstName;
    self.lastName = user.lastName;
    self.fullName = [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
    self.imageURLPhoto = user.imageURLPhoto;
}

@end
