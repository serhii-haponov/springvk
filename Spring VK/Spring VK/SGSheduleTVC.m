//
//  SGSheduleTVC.m
//  Spring VK
//
//  Created by Sergey on 3/20/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGSheduleTVC.h"
#import "SGCommonTVC.h"
#import "ViewController.h"
#import "SWRevealViewController.h"
#import "NavController.h"

#import "SGServerManager.h"

typedef NS_ENUM(NSUInteger, SGTypeCell) {
    SGTypeCellProfile,
    SGTypeCellFriends,
    SGTypeCellFollowers,
    SGTypeCellSubscriptions,
    SGTypeCellWall
};

@interface SGSheduleTVC ()

@property (strong, nonatomic) NSArray *arrayIdentyfiers;

@end

@implementation SGSheduleTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayIdentyfiers = @[@"sg_interface",
                              @"sg_friends",
                              @"sg_followers",
                              @"sg_subscriptions",
                              @"sg_wall"];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.arrayIdentyfiers count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = [self.arrayIdentyfiers objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < 1) {
        return 200.0;
    }
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row != SGTypeCellProfile) {
     
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        NSString *identifier;
        if (indexPath.row == SGTypeCellFollowers) {
            identifier = @"sg_friends";
        } else {
            identifier = [self.arrayIdentyfiers objectAtIndex:indexPath.row];
        }
        
        NavController *nc;
        
        if (indexPath.row == SGTypeCellSubscriptions || indexPath.row == SGTypeCellWall) {
            
            SGCommonTVC *vc = [storyBoard instantiateViewControllerWithIdentifier:identifier];
            nc = [[NavController alloc] initWithRootViewController:vc];
            vc.currentID = [SGServerManager sharedManager].accessToken.userId;
            vc.loadFromSideMenu = YES;
        } else {
            
            ViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:identifier];
            nc = [[NavController alloc] initWithRootViewController:vc];
            vc.currentID = [SGServerManager sharedManager].accessToken.userId;
            vc.tagButton = indexPath.row;
            vc.loadFromSideMenu = YES;
        }
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
}

@end
