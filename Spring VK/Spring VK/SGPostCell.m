//
//  SGPostCell.m
//  45_APITest
//
//  Created by Sergey on 12/28/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "SGPostCell.h"
#import "SGGroupWallViewController.h"

@implementation SGPostCell

+ (CGFloat)heightForRow:(NSString *)text {
    
    CGFloat offSet = 5.f;
    
    UIFont *font = [UIFont systemFontOfSize:17.f];
    
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowOffset = CGSizeMake(0, -1);
    shadow.shadowBlurRadius = 0.5;
    
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.alignment = NSTextAlignmentCenter;
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,       NSFontAttributeName,
                                paragraph,  NSParagraphStyleAttributeName,
                                shadow,     NSShadowAttributeName, nil];
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake((320 - offSet * 2), CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    return ceilf(CGRectGetHeight(rect)) + offSet * 2;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setup];
    
    [self.mainAvatarView.layer setCornerRadius:5.0f];
    [self.mainAvatarView.layer setMasksToBounds:YES];
    
    [self.postTextFieldView.layer setCornerRadius:3.0f];
    [self.postTextFieldView.layer setMasksToBounds:YES];

}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    NSString *stringPlaceHolder = @"Touch here for making post";;
    
    if ([textView.text isEqualToString:stringPlaceHolder]) {
        textView.text = @"";
        textView.textColor = [UIColor whiteColor];
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    
    if ([self.delegate respondsToSelector:@selector(postCell:)]) {
        
        [self.delegate postCell:self];
    }
}

#pragma mark - postTextFieldView.inputAccessoryView

-(void)setup {
    CGRect bounds = [UIScreen mainScreen].bounds;
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, bounds.size.width, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle: @"Send" style: UIBarButtonItemStylePlain target: self action: @selector(doneKeyboardClick)];
    [doneButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:20], NSForegroundColorAttributeName : [UIColor purpleColor]} forState: UIControlStateNormal];
    toolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton];
    [toolbar sizeToFit];
    self.postTextFieldView.inputAccessoryView = toolbar;
}

-(void)doneKeyboardClick{
    
    if ([self.delegate respondsToSelector:@selector(postCell:postText:)] && [self.postTextFieldView.text length] != 0) {
        [self.delegate postCell:self postText:self.postTextFieldView.text];
    }
    
    NSString *stringPlaceHolder = @"Touch here for making post";
    self.postTextFieldView.text = stringPlaceHolder;
    self.postTextFieldView.textColor = [UIColor colorWithWhite:1.f alpha:0.7];
    
    [self.postTextFieldView resignFirstResponder];
}

@end
