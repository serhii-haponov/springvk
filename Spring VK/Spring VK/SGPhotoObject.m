//
//  SGPhotoObject.m
//  45_APITest
//
//  Created by Sergey on 1/14/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGPhotoObject.h"
#import "NSDictionary+SGUrlFromString.h"

@implementation SGPhotoObject

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    self = [super init];
    if (self) {
        
        self.accessKey = [responseObject objectForKey:@"5a5ef9e409e743c8b7"];
        self.albumID = [responseObject objectForKey:@"-7"];
        self.creationDate = [self createDateFromSec:[responseObject objectForKey:@"created"]];
        self.height = [responseObject objectForKey:@"height"];
        self.width = [responseObject objectForKey:@"width"];
        self.ownerID = [responseObject objectForKey:@"owner_id"];
        self.photoID = [responseObject objectForKey:@"pid"];
        self.src = [responseObject makeUrlFromStringByKeyInDictionaty:@"src"];
        self.srcBig = [responseObject makeUrlFromStringByKeyInDictionaty:@"src_big"];
        self.srcSmall = [responseObject makeUrlFromStringByKeyInDictionaty:@"src_small"];
        self.srcXbig = [responseObject makeUrlFromStringByKeyInDictionaty:@"src_xbig"];
        self.srcXxbig = [responseObject makeUrlFromStringByKeyInDictionaty:@"src_big"];
        
        self.text = [responseObject objectForKey:@"text"];
        self.userID = [responseObject objectForKey:@"user_id"];
    }
    return self;
}

@end
