//
//  SGPhotoController.m
//  45_APITest
//
//  Created by Sergey on 12/9/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "SGPhotoController.h"
#import "ViewController.h"
#import "SGCommonTVC.h"
#import "SWRevealViewController.h"

#import "UIImageView+AFNetworking.h"
#import "SGServerManager.h"

#import "SGUser.h"

@interface SGPhotoController ()

@property (strong, nonatomic) SGUser *currentUser;
@property (assign, nonatomic) NSInteger tagButton;

@end

@implementation SGPhotoController

- (void)viewDidLoad {
    [super viewDidLoad];

     [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    if (self.person) {
        [self setUserParametersToView:self.person];
        self.currentUser = self.person;

    } else {
        
        UIImage *image = [UIImage imageNamed:@"menu.png"];
        UIBarButtonItem *sandwichButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
        self.navigationItem.leftBarButtonItem = sandwichButton;
        
        [[SGServerManager sharedManager] authorizeUser:^(SGUser *user) {
            NSLog(@"AUTHORIZED");
            self.currentUser = user;
            [self setUserParametersToView:self.currentUser];
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"TailingProfIm.png"]];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue destinationViewController] isKindOfClass:[ViewController class]]) {
        
        ViewController *tvc = [segue destinationViewController];
        tvc.currentID = self.currentUser.userID;
        tvc.tagButton = self.tagButton;
        
    } else {
        SGCommonTVC *tvc = [segue destinationViewController];
        tvc.currentID = self.currentUser.userID;
    }
}

#pragma mark - Action button

- (IBAction)friendsButton:(UIButton *)sender {
    self.tagButton = sender.tag;
}

- (IBAction)actionFollowersButton:(UIButton *)sender {
    self.tagButton = sender.tag;
}

#pragma mark - Own methods

- (void)setUserParametersToView:(SGUser *)user {
    
    self.nameUser.text = user.firstName;
    self.familyUser.text = user.lastName;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:user.imageURLBig];
    
    void(^block)(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) = ^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        
        self.photoUser.image = image;
        self.photoUser.layer.cornerRadius = 10;
    };
    
    UIImage *imagePlaceHolder = [UIImage imageNamed:@"default.png"];
    
    [self.photoUser setImageWithURLRequest:request placeholderImage:imagePlaceHolder success:block failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
    }];
}

#pragma mark - Orientations

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskAll;
}

@end
