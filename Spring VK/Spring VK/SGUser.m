//
//  SGUser.m
//  45_APITest
//
//  Created by Sergey on 11/27/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "SGUser.h"

@implementation SGUser

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super initWithServerResponse:responseObject];
    if (self) {
        self.userID = [responseObject objectForKey:@"uid"];
        self.firstName = [responseObject objectForKey:@"first_name"];
        self.lastName = [responseObject objectForKey:@"last_name"];
        self.online = [[responseObject objectForKey:@"online"] boolValue];
       
        NSString *urlString50 = [responseObject objectForKey:@"photo_50"];
        if (urlString50) {
        self.imageURL50 = [NSURL URLWithString:urlString50];
        }
        
        NSString *urlStringPhoto = [responseObject objectForKey:@"photo"];
        if (urlStringPhoto) {
            self.imageURLPhoto = [NSURL URLWithString:urlStringPhoto];
        }
        
        NSString *urlStringBig = [responseObject objectForKey:@"photo_big"];
        if (urlStringBig) {
            self.imageURLBig = [NSURL URLWithString:urlStringBig];
        }
    }
    return self;
}

#pragma mark - SGSubscriptionProtocol

- (NSString *)name {
    
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
    
    return fullName;
}

- (NSURL *)avatarURL {
    return self.imageURL50;
}

@end
