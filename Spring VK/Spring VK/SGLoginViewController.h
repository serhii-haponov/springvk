//
//  SGLoginViewController.h
//  45_APITest
//
//  Created by Sergey on 12/5/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SGAccessToken;

typedef void(^SGLoginComplationBlock)(SGAccessToken *token);

@interface SGLoginViewController : UIViewController

- (id)initWithComplationBlock:(SGLoginComplationBlock)complationBlock;

@end

