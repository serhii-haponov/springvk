//
//  SGVideoObject.h
//  45_APITest
//
//  Created by Sergey on 1/18/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGServerObject.h"

@interface SGVideoObject : SGServerObject

@property (strong, nonatomic) NSString   *date;

@property (strong, nonatomic) NSString *accessKey;
@property (strong, nonatomic) NSString *ownerID;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *descriptionVideo;

@property (strong, nonatomic) NSNumber *duration;
@property (strong, nonatomic) NSNumber *videoID;
@property (strong, nonatomic) NSNumber *views;

@property (strong, nonatomic) NSURL *image;
@property (strong, nonatomic) NSURL *imageBig;
@property (strong, nonatomic) NSURL *imageSmall;

@end
