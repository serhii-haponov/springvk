//
//  SGProfileCell.h
//  Spring VK
//
//  Created by Sergey on 4/24/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGProfileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userAvatar;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end
