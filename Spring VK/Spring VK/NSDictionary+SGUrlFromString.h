//
//  NSDictionary+SGUrlFromString.h
//  45_APITest
//
//  Created by Sergey on 1/25/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SGUrlFromString)

- (NSURL *)makeUrlFromStringByKeyInDictionaty:(NSString *)key;

@end
