//
//  SGLikeRepost.h
//  45_APITest
//
//  Created by Sergey on 1/31/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SGLikeRepostDelegate;

@interface SGLikeRepost : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imageViewLike;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRepost;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@property (weak, nonatomic) IBOutlet UILabel *quantityComments;
@property (weak, nonatomic) IBOutlet UILabel *quantityLikes;
@property (weak, nonatomic) IBOutlet UILabel *quantityReposts;

@property (weak, nonatomic) id<SGLikeRepostDelegate> delegate;
@property (assign, nonatomic) NSInteger indexButton;


- (IBAction)actionCommentButton:(UIButton *)sender;

@end

@protocol SGLikeRepostDelegate <NSObject>

- (void)likeRepostComment:(SGLikeRepost *)comment tagButton:(NSInteger)tagB;

@end
