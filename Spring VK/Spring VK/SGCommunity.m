//
//  SGGroup.m
//  45_APITest
//
//  Created by Sergey on 1/14/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGCommunity.h"

@implementation SGCommunity 

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    self = [super init];
    if (self) {
        self.gID = [responseObject objectForKey:@"gid"];
        self.name = [responseObject objectForKey:@"name"];
        self.photoUrl = [responseObject objectForKey:@"photo_100"];
    }
    return self;
}

#pragma mark - SGSubscriptionProtocol

- (NSString *)name {
    return _name;
}

- (NSURL *)avatarURL {
    NSURL *url = [NSURL URLWithString:self.photoUrl];
    return url;
}


@end
