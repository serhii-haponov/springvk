//
//  SGCommantCell.m
//  Spring VK
//
//  Created by Sergey on 2/24/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGCommantCell.h"
#import "UIImageView+AFNetworking.h"

@implementation SGCommantCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCommentSetObjs:(SGComment *)commentSetObjs {
    _commentSetObjs = commentSetObjs;
    
    self.imageUserCell.layer.masksToBounds = YES;
    self.imageUserCell.layer.cornerRadius = CGRectGetWidth(self.imageUserCell.bounds) / 2;
    [self.imageUserCell setImageWithURL:commentSetObjs.imageURLPhoto];
    
    self.labelUserName.text = commentSetObjs.fullName;
    self.labelUserComment.text = commentSetObjs.commentText;
    self.labelUserDate.text = commentSetObjs.commentDate;
}

@end
