//
//  SGCommetnsVC.h
//  Spring VK
//
//  Created by Sergey on 2/27/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGCommetnsVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintTextView;

@property (strong, nonatomic) NSString *ownerCommtentsID;
@property (strong, nonatomic) NSString *postCommtentsID;
@property (assign, nonatomic) NSUInteger quantityComments;

@end
