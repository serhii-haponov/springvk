//
//  SGServerObject.m
//  45_APITest
//
//  Created by Sergey on 12/27/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "SGServerObject.h"
#import "SGUser.h"
#import "SGCommunity.h"

@implementation SGServerObject

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super init];
    if (self) {
        
    }
    return self;
}

+ (SGServerObject *)factoryWithResponse:(NSDictionary *)responseObject {
    id uid = [responseObject objectForKey:@"uid"];
    id gid = [responseObject objectForKey:@"gid"];
    
    if (uid) {
        SGUser *user = [[SGUser alloc] initWithServerResponse: responseObject];
        return user;
    } else if (gid) {
        SGCommunity *community = [[SGCommunity alloc] initWithServerResponse: responseObject];
        return community;
    } else {
        SGServerObject *object = [[SGServerObject alloc] initWithServerResponse: responseObject];
        return object;
    }
}

+ (NSString *)timeFormatConvertToSeconds:(NSNumber *)timeSecs {
    int totalSeconds = [timeSecs intValue];
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

- (NSString *)createDateFromSec:(NSNumber *)second {

    NSTimeInterval interval = [second doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970: interval];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd' at 'hh:mm aa"];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}


@end
