//
//  SGServerManager.h
//  45_APITest
//
//  Created by Sergey on 11/16/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SGPost.h"
#import "SGComment.h"
#import "SGUser.h"
#import "SGAccessToken.h"

@interface SGServerManager : NSObject

@property (strong, nonatomic) SGUser *usersFriends;
@property (strong, nonatomic) SGUser *userInfo;
@property (strong, nonatomic) SGPost *wallPost;
@property (strong, nonatomic) SGAccessToken *accessToken;
@property (assign, nonatomic) NSInteger quantityPosts;
@property (assign, nonatomic) NSInteger quantityFriends;
@property (assign, nonatomic) NSInteger quantityFollowers;
@property (assign, nonatomic) NSInteger quantitySubsctiptions;

+ (SGServerManager *)sharedManager;

- (void)authorizeUser:(void(^)(SGUser *user))completion;

- (void)getUser:(NSString *)userId
      onSuccess:(void(^)(SGUser *user))success
      onFailure:(void(^)(NSError *error, NSInteger statuseCode))failure;

- (void)getFriendsWithUserID:(NSString *)userId
                      Offset:(NSInteger)offSet
                       count:(NSInteger)count
                   onSuccess:(void(^)(NSArray *friends))success
                   onFailure:(void(^)(NSError *error, NSInteger statuseCode))failure;

- (void)getSubscriptionsUserID:(NSString *)userID
                    withOffset:(NSInteger)offset
                      andCount:(NSInteger)count
                     onSuccess:(void(^)(NSArray *subscriptions))success
                     onFailure:(void(^)(NSError *error, NSInteger statusCode))failure;

- (void)getFollowersUserID:(NSString *)userID
                withOffset:(NSInteger)offset
                  andCount:(NSInteger)count
                 onSuccess:(void(^)(NSArray *followers))success
                 onFailure:(void(^)(NSError *error, NSInteger statusCode))failure;

- (NSURLSessionDataTask *)getGroupWall:(NSString *)groupId
                          withOffSet:(NSInteger)offSet
                            andCount:(NSInteger)count
                           onSuccess:(void(^)(NSArray *post))success
                           onFailure:(void(^)(NSError *error, NSInteger statuseCode)) failure;

- (NSURLSessionDataTask *)getOwnersWallComments:(NSString *)ownerID
                                      ownerPost:(NSString *)postID
                                      withCount:(NSInteger)count
                                      andOffset:(NSInteger)offset
                                      inSuccess:(void(^)(NSArray *comments))success
                                      inFailure:(void(^)(NSError *error, NSInteger statusCode))failure;

- (void)postText:(NSString *)text
        forGroup:(NSString *)groupID
       inSuccess:(void(^)(id result))success
       inFailure:(void(^)(NSError *error, NSInteger statusCode))failure;

- (void)createComment:(NSString *)text
             inWallID:(NSString *)groupID
            forPostID:(NSString *)postID
            inSuccess:(void(^)(id result))success
            inFailure:(void(^)(NSError *error, NSInteger statusCode))failure;

- (void)getVideosByIds:(NSArray *)vids ownersIds:(NSArray *)ownerIds
              forGroup:(NSString *)groupID
       inSuccess:(void(^)(id result))success
       inFailure:(void(^)(NSError *error, NSInteger statusCode))failure;

@end
