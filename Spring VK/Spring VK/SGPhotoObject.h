//
//  SGPhotoObject.h
//  45_APITest
//
//  Created by Sergey on 1/14/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGServerObject.h"

@interface SGPhotoObject : SGServerObject

@property (strong, nonatomic) NSString *accessKey;
@property (strong, nonatomic) NSString *albumID;
@property (strong, nonatomic) NSString *creationDate;
@property (strong, nonatomic) NSString *ownerID;
@property (strong, nonatomic) NSString *photoID;
@property (strong, nonatomic) NSString *postID;
@property (strong, nonatomic) NSURL *src;
@property (strong, nonatomic) NSURL *srcBig;
@property (strong, nonatomic) NSURL *srcSmall;
@property (strong, nonatomic) NSURL *srcXbig;
@property (strong, nonatomic) NSURL *srcXxbig;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSNumber *height;
@property (strong, nonatomic) NSNumber *width;

@end
