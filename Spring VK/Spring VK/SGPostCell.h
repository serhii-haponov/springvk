//
//  SGPostCell.h
//  45_APITest
//
//  Created by Sergey on 12/28/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SGPostCellDelegate;

@interface SGPostCell : UITableViewCell <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *mainAvatarView;
@property (weak, nonatomic) IBOutlet UITextView *postTextFieldView;
@property (weak, nonatomic) id<SGPostCellDelegate> delegate;

+ (CGFloat)heightForRow:(NSString *)text;

@end

@protocol SGPostCellDelegate <NSObject>

@optional
- (void)postCell:(SGPostCell *)cell;
- (void)postCell:(SGPostCell *)cell postText:(NSString *)text;

@end
