//
//  SGAvatarDate.h
//  45_APITest
//
//  Created by Sergey on 2/1/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGAvatarDate : UIView

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *dateCreation;
@property (weak, nonatomic) IBOutlet UILabel *ownerName;

@end
