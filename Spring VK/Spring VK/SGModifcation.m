//
//  SGModifcation.m
//  45_APITest
//
//  Created by Sergey on 1/9/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGModifcation.h"

@implementation SGModifcation

+ (UIImage *)resizingPlaceHolder:(UIImage *)image withSize:(NSInteger)size {
    
    CGRect rect = CGRectMake(0,0,size,size);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImagePNGRepresentation(picture1);
    UIImage *img = [UIImage imageWithData:imageData];
    
    return img;
}

@end
