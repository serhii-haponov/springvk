//
//  SGPost.h
//  45_APITest
//
//  Created by Sergey on 12/27/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "SGServerObject.h"

@class SGAttachment;

@interface SGPost : SGServerObject

@property (strong, nonatomic) NSNumber *itemID;
@property (strong, nonatomic) NSNumber *fromID;
@property (strong, nonatomic) NSNumber *toID;

@property (strong, nonatomic) NSString *creationDate;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *postType;

@property (strong, nonatomic) SGAttachment *postAttachment;

@property (strong, nonatomic) NSNumber *postCountComents;
@property (strong, nonatomic) NSNumber *postCountLikes;
@property (strong, nonatomic) NSNumber *postCountReposts;

@property (strong, nonatomic) NSNumber *userLikes;
@property (strong, nonatomic) NSNumber *userReposted;

@property (strong, nonatomic) NSString *ownerName;
@property (strong, nonatomic) NSURL *photoFromID;

- (void)receavePhotoUrlFromID:(NSDictionary *)groupAndProfil;

@end
