//
//  SGModifcation.h
//  45_APITest
//
//  Created by Sergey on 1/9/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIGraphics.h>
#import <UIKit/UIImage.h>

@interface SGModifcation : NSObject

+ (UIImage *)resizingPlaceHolder:(UIImage *)image withSize:(NSInteger)size;

@end
