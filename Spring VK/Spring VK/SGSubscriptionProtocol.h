//
//  SGSubscriptionProtocol.h
//  45_APITest
//
//  Created by Sergey on 1/14/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SGCommunity;
@class SGUser;

@protocol SGSubscriptionProtocol <NSObject>

@required
- (NSString *)name;
- (NSURL *)avatarURL;
@end
