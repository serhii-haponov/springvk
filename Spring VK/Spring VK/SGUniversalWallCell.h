//  SGUniversalWallCell.h
//  45_APITest
//
//  Created by Sergey on 1/29/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SGPost.h"
#import "SGLikeRepost.h"

@interface SGUniversalWallCell : UITableViewCell

@property (strong, nonatomic) SGPost *cellPost;
@property (strong, nonatomic) SGLikeRepost *likeRepostView;

- (CGFloat)heightForRow:(SGPost *)post;

@end
