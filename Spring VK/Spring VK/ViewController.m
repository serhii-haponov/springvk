//
//  ViewController.m
//  45_APITest
//
//  Created by Sergey on 11/16/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "ViewController.h"
#import "SGPhotoController.h"
#import "SWRevealViewController.h"

#import "SGServerManager.h"
#import "SGFriendCell.h"

@interface SGSectionOfPersons : NSObject

@property (strong, nonatomic) NSString *sectionName;
@property (strong, nonatomic) NSMutableArray *personsInSection;

@end

@implementation SGSectionOfPersons

- (instancetype)init {
   self =  [super init];
    if (self) {
        self.personsInSection = [NSMutableArray  new];
    }
    return self;
}

@end

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *arrayPerson;
@property (strong, nonatomic) NSArray *arraySection;
@property (nonatomic, assign) CGFloat lastContentOffset;
@property (strong, nonatomic) NSInvocationOperation *sortOperation;

@property (strong, nonatomic) NSSortDescriptor *sortDescriptorName;
@property (strong, nonatomic) NSSortDescriptor *sortDescriptorSurname;
@property (strong, nonatomic) NSSortDescriptor *sortDescriptorOnline;

@end

@implementation ViewController

static NSInteger personsInRequest = 10000;

- (void)viewDidLoad {
    [super viewDidLoad];

    self.arraySection = [NSArray new];
//    id<SGUserProtocol> item;
    
    self.sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES];
    self.sortDescriptorSurname = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
    self.sortDescriptorOnline = [[NSSortDescriptor alloc] initWithKey:@"online" ascending:NO];
    
    __weak ViewController *weakSelf = self;
    
    void (^block)(void) = ^{
        
         weakSelf.arraySection = [weakSelf generateSection:weakSelf.arrayPerson byFirstName:YES withFilter:weakSelf.searchBar.text];
    };
    
    switch (self.tagButton) {
        case SGKindOfButtonFriends:
        {
            [self getFriendsFromServerUserId:self.currentID withBlock:block];
            break;
        }
        case SGKindOfButtonFollowers:
        {
            [self getFollowersFromServerUserId:self.currentID withBlock:block];
            break;
        }
        default:
            break;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    if (self.loadFromSideMenu) {
        UIImage *image = [UIImage imageNamed:@"menu.png"];
        UIBarButtonItem *sandwichButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
        self.navigationItem.leftBarButtonItem = sandwichButton;
    }
}

- (NSArray *) generateSection:(NSArray *)array byFirstName:(BOOL)sort withFilter:(NSString *)filterString {
    
    NSMutableArray *sortedArray = [NSMutableArray new];
    NSString *currentLetter = nil;
    
    for (SGUser *user in array) {
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
      
        if ([filterString length] > 0 && [fullName rangeOfString:filterString].location ==NSNotFound) {
            continue;
        }
        NSString *firstLatter;
        if (sort) {
            firstLatter = [fullName substringToIndex:1];
        } else {
            firstLatter = [user.lastName substringToIndex:1];
        }
        SGSectionOfPersons *section = nil;
        
        if (![firstLatter isEqualToString:currentLetter]) {
            
            section = [[SGSectionOfPersons alloc] init];
            section.sectionName = [NSString stringWithFormat:@"%@:", firstLatter];
            currentLetter = firstLatter;
            
            [sortedArray addObject:section];
        }
        section = [sortedArray lastObject];
        [section.personsInSection addObject:user];
    }
    return sortedArray;
}

- (void)sortByNamesInBackground {
     self.arrayPerson = [self.arrayPerson sortedArrayUsingDescriptors:@[self.sortDescriptorName, self.sortDescriptorSurname, self.sortDescriptorOnline]];
    self.arraySection  = [self generateSection:self.arrayPerson byFirstName:YES withFilter:self.searchBar.text];
    
    __weak ViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.tableView reloadData];
    });
}

- (void)sortBySurNamesInBackground {
    self.arrayPerson = [self.arrayPerson sortedArrayUsingDescriptors:@[self.sortDescriptorSurname, self.sortDescriptorName, self.sortDescriptorOnline]];
    self.arraySection  = [self generateSection:self.arrayPerson byFirstName:NO withFilter:self.searchBar.text];
    
    __weak ViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.tableView reloadData];
    });
}

- (void)sortByOnlineInBackground {
    self.arrayPerson = [self.arrayPerson sortedArrayUsingDescriptors:@[self.sortDescriptorOnline, self.sortDescriptorName, self.sortDescriptorSurname]];
    self.arraySection  = [self generateSection:self.arrayPerson byFirstName:YES withFilter:self.searchBar.text];
    
    __weak ViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.tableView reloadData];
    });
}


#pragma mark - API methods

- (void)getFriendsFromServerUserId:(NSString *)userId withBlock:(void(^)(void))completion {
    
    [[SGServerManager sharedManager]
     getFriendsWithUserID:userId Offset:0 count:personsInRequest onSuccess:^(NSArray *friends) {
         self.arrayPerson = friends;
   
         if (completion) {
             completion();
         }
         
         [self.tableView reloadData];
     }
     onFailure:^(NSError *error, NSInteger statuseCode) {
         NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statuseCode);
     }];
}

- (void)getFollowersFromServerUserId:(NSString *)userId withBlock:(void(^)(void))completion {
    
    [[SGServerManager sharedManager] getFollowersUserID:userId withOffset:0 andCount:personsInRequest onSuccess:^(NSArray *followers) {
        
        self.arrayPerson = followers;

        self.arrayPerson = [self.arrayPerson sortedArrayUsingDescriptors:@[self.sortDescriptorName, self.sortDescriptorSurname]];
        
        if (completion) {
            completion();
        }
        
        [self.tableView reloadData];
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.arraySection count];
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    SGSectionOfPersons *sectionPersons = [self.arraySection objectAtIndex:section];
    
    return sectionPersons.sectionName;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    SGSectionOfPersons *sectionPersons = [self.arraySection objectAtIndex:section];
    
    return [sectionPersons.personsInSection count];
}

- (nullable NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (SGSectionOfPersons *section in self.arraySection) {
        
        [array addObject:section.sectionName];
    }
    return array;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SGFriendCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    SGSectionOfPersons *section = [self.arraySection objectAtIndex:indexPath.section];
    SGUser *person = [section.personsInSection objectAtIndex:indexPath.row];
    [cell setupWithUser:person];
    
    if (![self.searchBar.text  isEqual: @""]) {
        
        NSAttributedString *attrStr = [self highlightText:cell.nameLabel.text bySearch:self.searchBar.text];
        
        cell.nameLabel.attributedText = attrStr;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SGPhotoController *photoVC = [storyboard instantiateViewControllerWithIdentifier:@"sg_interface"];
    
    SGSectionOfPersons *section = [self.arraySection objectAtIndex:indexPath.section];
    SGUser *chosenPerson = [section.personsInSection objectAtIndex:indexPath.row];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    photoVC.person = chosenPerson;
    [self.navigationController pushViewController:photoVC animated:YES];
}

#pragma mark - UIScrollViewDelegate


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    self.lastContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    
    if (self.lastContentOffset < scrollView.contentOffset.y) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    } else if (self.lastContentOffset > scrollView.contentOffset.y) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    self.arraySection = [self generateSection:self.arrayPerson byFirstName:YES withFilter:searchText];
    
    [self.tableView reloadData];
}

#pragma mark - SegmentedControlAction

- (IBAction)changeSegmControl:(UISegmentedControl *)sender {
    
    if ([self.arrayPerson count] != 0) {
        
        switch (sender.selectedSegmentIndex) {
            case SGSortDescriptionName:
                
                [self.sortOperation cancel];
                
                self.sortOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(sortByNamesInBackground) object:nil];
                
                [self.sortOperation start];
                
                break;
                
            case SGSortDescriptionSurname:
                
                [self.sortOperation cancel];
                
                self.sortOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(sortBySurNamesInBackground) object:nil];
                
                [self.sortOperation start];
                
                break;
                
            case SGSortDescriptionOnline:
                
                [self.sortOperation cancel];
                
                self.sortOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(sortByOnlineInBackground) object:nil];
                
                [self.sortOperation start];
                
                break;
                
            default:
                break;
        }
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView scrollToRowAtIndexPath:myIP atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (NSAttributedString *)highlightText:(NSString *)text bySearch:(NSString *)search {
    
    NSString *searchTerm = search;
    NSString *resultText = text;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:resultText];
    
    NSString *pattern = [NSString stringWithFormat:@"(%@)", searchTerm];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:kNilOptions error:nil];
    NSRange range = NSMakeRange(0, resultText.length);
    
    [regex enumerateMatchesInString:resultText options:kNilOptions range:range usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
        
        NSRange subStringRange = [result rangeAtIndex:1];
        
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor redColor]
                                 range:subStringRange];
    }];
    return attributedString;
}

@end










