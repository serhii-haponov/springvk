//
//  SGAccessToken.h
//  45_APITest
//
//  Created by Sergey on 12/5/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SGAccessToken : NSObject

@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSDate *expirationDate;
@property (strong, nonatomic) NSString *userId;

@end
