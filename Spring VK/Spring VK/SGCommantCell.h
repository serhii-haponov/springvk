//
//  SGCommantCell.h
//  Spring VK
//
//  Created by Sergey on 2/24/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SGComment.h"

@interface SGCommantCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageUserCell;
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelUserComment;
@property (weak, nonatomic) IBOutlet UILabel *labelUserDate;

@property (strong, nonatomic) SGComment *commentSetObjs;

@end
