 //
//  SGLoginViewController.m
//  45_APITest
//
//  Created by Sergey on 12/5/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "SGLoginViewController.h"
#import "SGAccessToken.h"

@interface SGLoginViewController () <UIWebViewDelegate>

@property (copy, nonatomic) SGLoginComplationBlock complationBlock;
@property (weak, nonatomic) UIWebView *webView;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;

@end

@implementation SGLoginViewController

- (id)initWithComplationBlock:(SGLoginComplationBlock)complationBlock {
    
    self = [super init];
    if (self) {
        self.complationBlock = complationBlock;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.indicator = [[UIActivityIndicatorView alloc] init];
    self.indicator.center = self.navigationController.view.center;
    [self.navigationController.view addSubview:self.indicator];
    self.indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    self.indicator.color = [UIColor blackColor];
    [self.indicator startAnimating];
    
    CGRect r = self.view.bounds;
    r.origin = CGPointZero;
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:r];
    
    webView.autoresizingMask =
    UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    [self.view addSubview:webView];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(actionCancel:)];
    
    self.navigationItem.rightBarButtonItem = item;
    self.navigationItem.title = @"Login";
    
    NSString *urlString = @"https://oauth.vk.com//authorize?"
                           "client_id=5761006&"
                           "scope=996382&" // 2 + 4 + 16 +; with offline 996382 without 930846
                           "redirect_uri=https://oauth.vk.com/blank.html&"
                           "display=mobile&"
                           "V=5.6&"
                           "response_type=token";
    
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    webView.delegate = self; 
    
    [webView loadRequest:request];
    
    self.webView = webView;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.indicator stopAnimating];
}



- (void)dealloc {
    
    self.webView.delegate = nil;
}

#pragma mark - Actions

- (void)actionCancel:(UIBarButtonItem *)item {
    
    if (self.complationBlock) {
        self.complationBlock(nil);
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

http://hello.there/#access_token=a83dc060b9c494221247b1ac5244a50e0bbd6ecc5eb7de6696a84c172503d71b8f5197de00c1cf49ed334&expires_in=86400&user_id=39229810
    
    if ([[[request URL] absoluteString] rangeOfString:@"#access_token="].location != NSNotFound) {
    
        SGAccessToken *token = [[SGAccessToken alloc] init];
        
        NSString *query = [[request URL] description];
        NSArray *array = [query componentsSeparatedByString:@"#"];
        
        if ([array count] > 1) {
            query = [array lastObject];
        }
        
        NSArray *pairs = [query componentsSeparatedByString:@"&"];
        
        for (NSString *pair in pairs) {
            
            NSArray *values = [pair componentsSeparatedByString:@"="];
            
            if ([values count] == 2) {
                
                NSString *key = [values firstObject];
                
                if ([key isEqualToString:@"access_token"]) {
                    token.token = [values lastObject];
                } else if ([key isEqualToString:@"expires_in"]) {
                       
                    NSTimeInterval interval = [[values lastObject] doubleValue];
                    
                    token.expirationDate = [NSDate dateWithTimeIntervalSinceNow:interval];
                } else if ([key isEqualToString:@"user_id"])
                    
                    token.userId = [values lastObject];
            }
        }
        
        if (self.complationBlock) {
            self.complationBlock(token);
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
                
        return NO;
    }
    return YES;
}

@end
