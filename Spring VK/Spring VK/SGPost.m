//
//  SGPost.m
//  45_APITest
//
//  Created by Sergey on 12/27/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "SGPost.h"
#import "SGAttachment.h"

@implementation SGPost

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    
    self = [super init];
    if (self) {
        self.itemID = [responseObject objectForKey:@"id"];
        self.fromID = [responseObject objectForKey:@"from_id"];
    
        self.creationDate = [self createDateFromSec:[responseObject objectForKey:@"date"]];
        
        NSString *rawText = [responseObject objectForKey:@"text"];
        self.text = [rawText stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        
        self.toID = [responseObject objectForKey:@"to_id"];
        self.postType = [responseObject objectForKey:@"post_type"];
        
        NSArray *attacmentsData = [responseObject objectForKey:@"attachments"];
        if (attacmentsData) {
            self.postAttachment = [[SGAttachment alloc] initWithServerResponse:responseObject];
        } else {
            self.postAttachment = nil;
        }
        
        self.postCountComents = [[responseObject objectForKey:@"comments"] objectForKey:@"count"];
        self.postCountLikes = [[responseObject objectForKey:@"likes"] objectForKey:@"count"];
        self.userLikes = [[responseObject objectForKey:@"likes"] objectForKey:@"user_likes"];
        self.postCountReposts = [[responseObject objectForKey:@"reposts"] objectForKey:@"count"];
        self.userReposted = [[responseObject objectForKey:@"reposts"] objectForKey:@"user_reposted"];
    }
    return self;
}

- (void)receavePhotoUrlFromID:(NSDictionary *)groupAndProfil {
    
    NSArray *groupsArray = [groupAndProfil objectForKey:@"groups"];
    NSArray *profilesArray = [groupAndProfil objectForKey:@"profiles"];
    
    if ([[self.fromID stringValue] hasPrefix:@"-"]) {
        
        for (NSDictionary *strID in groupsArray) {
            NSMutableString *strFromID = [NSMutableString stringWithString:[self.fromID stringValue]];
            NSRange range = NSMakeRange(0, 1);
            [strFromID deleteCharactersInRange:range];
            
            NSNumber *gID = [strID objectForKey:@"gid"];
            if ([[gID stringValue] isEqualToString:strFromID]) {
                self.photoFromID = [NSURL URLWithString:[strID objectForKey:@"photo"]];
                self.ownerName = [strID objectForKey:@"name"];
                break;
            }
        }
    } else {
        
        for (NSDictionary *strID in profilesArray) {
            NSNumber *pID = [strID objectForKey:@"uid"];
            if ([[pID stringValue] isEqualToString:[self.fromID stringValue]]) {
                self.photoFromID = [NSURL URLWithString:[strID objectForKey:@"photo"]];
                NSString *strName = [NSString stringWithFormat:@"%@ %@", [strID objectForKey:@"first_name"], [strID objectForKey:@"last_name"]];
                self.ownerName = strName;
                break;
            }
        }
    }
}

@end
