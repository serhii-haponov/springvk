//
//  SGUniversalWallCell.m
//  45_APITest
//
//  Created by Sergey on 1/29/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGUniversalWallCell.h"
#import "UIKit+AFNetworking.h"

#import "SGAttachment.h"
#import "SGPhotoObject.h"
#import "SGAudioObject.h"
#import "SGVideoObject.h"
#import "SGDocumentObject.h"

#import "SGAvatarDate.h"
#import "SGMusicConteiner.h"

const NSInteger SGConstraintHorizontalOffset = 20;
const NSInteger SGConstraintVerticalOffset = 5;
const NSInteger SGContentOfContentViewHorisontalOffset = 16;

const NSInteger SGPreferredHeightForImageInRow_250 = 250;
const NSInteger SGPreferredHeightForImageInRow_120 = 120;
const CGFloat SGOffsetBetweenObjects_15 = 15;
const CGFloat SGAvatarViewHeight = 50;
const CGFloat SGLikeComentViewHeight = 50;
const CGFloat SGMusicConteierHeight = 50;
const CGFloat SGDocumentIconHeight = 50;
const CGFloat SGStartTopOffset = 10;
const CGFloat SGEndTopOffset = 15;

@interface SGUniversalWallCell ()

@property (strong, nonatomic) NSMutableArray *imageViews;

@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) UILabel *stringLabel;
@property (strong, nonatomic) SGAvatarDate *avatarDateView;
@property (assign, nonatomic) CGFloat widthScreenOfDevice;
@property (assign, nonatomic) CGFloat boundsForContentOfContentView;
@property (assign, nonatomic) NSInteger preferredHeightForImageInRow;

@end

@implementation SGUniversalWallCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    
        self.containerView = [[UIView alloc] init];
        [self.contentView addSubview:self.containerView];
        
        self.containerView.backgroundColor = [UIColor whiteColor];
        self.containerView.layer.masksToBounds = YES;
        self.containerView.layer.cornerRadius = 5.0f;
//        self.containerView.layer.shadowOffset = CGSizeMake(1, 1);
//        self.containerView.layer.shadowOpacity = 0.5;
//        self.containerView.layer.shadowRadius = 0.5;
        
//        self.containerView.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
        
        self.containerView.translatesAutoresizingMaskIntoConstraints = NO;

        [self addConstraintFromView:self.containerView toView:self.contentView leftSpace:SGConstraintHorizontalOffset / 2 rightSpace:SGConstraintHorizontalOffset / 2 topSpace:SGConstraintVerticalOffset bottomSpace:SGConstraintVerticalOffset];
        
        self.imageViews = [NSMutableArray array];
        
        self.contentView.backgroundColor = [UIColor colorWithRed:0.90 green:0.90 blue:0.90 alpha:1];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setupCellMetricks];
        
        NSInteger topOffset = SGStartTopOffset;
        
        SGAvatarDate *avatarView = [[[NSBundle mainBundle] loadNibNamed:@"AvatarAndDate" owner:self options:nil] firstObject];
        [self.containerView addSubview:avatarView];
        
        CGRect rectAvatar = CGRectMake(0, topOffset, self.widthScreenOfDevice, SGAvatarViewHeight);
        
        avatarView.frame = rectAvatar;
        avatarView.avatar.layer.masksToBounds = YES;
        avatarView.avatar.layer.cornerRadius = SGAvatarViewHeight / 2;
        self.avatarDateView = avatarView;
       
        SGLikeRepost *likeView = [[[NSBundle mainBundle] loadNibNamed:@"LabelLikeRepost" owner:self options:nil] firstObject];
        [self.containerView addSubview:likeView];
        
        self.likeRepostView = likeView;
        [self likeRepostConstraints];
    }
    return self;
}

- (CGFloat)heightForRow:(SGPost *)post {
    CGFloat totalHeight =
    SGStartTopOffset +
    SGAvatarViewHeight +
    SGOffsetBetweenObjects_15 +
    SGLikeComentViewHeight +
    SGEndTopOffset;
    
    if (![post.text isEqualToString:@""]) {
        
        CGFloat heightText = [self countHeightLable:post.text];
        totalHeight += heightText;
    }
    
    if (post.postAttachment) {
        
        NSArray *arrayAttachmetTyeps = post.postAttachment.attachmentTypesInOrder;
        NSArray *cleanedArrayAttachmetTyeps = [[NSSet setWithArray:arrayAttachmetTyeps ] allObjects];
        
        NSArray *arrayAttachmentPhoto = post.postAttachment.arrayPhotoObj;
        NSArray *arrayAttachmentVideo = post.postAttachment.arrayVideoObj;
        NSArray *arrayAttachmentMusic = post.postAttachment.arrayAudioObj;
        NSArray *arrayAttachmentDoc = post.postAttachment.arrayDocumentObj;
        
        for (NSNumber *number in cleanedArrayAttachmetTyeps) {
            
            NSUInteger typeAttachment = [number unsignedIntegerValue];
            
            switch (typeAttachment) {
                case SGAttachmentPhotos:
                    
                   totalHeight += [self heightConteinerImages:arrayAttachmentPhoto];
                    break;
                case SGAttachmentVideo:
                    
                    totalHeight += [self heightConteinerImages:arrayAttachmentVideo];
                    break;
                case SGAttachmentAudio:
                    
                    for (int i = 0; i < [arrayAttachmentMusic
                                         count]; i++) {
                        
                        totalHeight += SGMusicConteierHeight;
                    }
                    break;
                case SGAttachmentDoc:
                    
                   totalHeight += [self heightConteinerImages:arrayAttachmentDoc];
                    break;
                case SGAttachmentUndefined:
                    
//                    NSLog(@"Do not find right identifier");
                    break;
                    
                default:
                    break;
            }
        }
    }
    return totalHeight;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    
    [self.imageViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.imageViews removeAllObjects];
    
    [self.stringLabel removeFromSuperview];
}

- (void)setCellPost:(SGPost *)cellPost {
    [self setupCellMetricks];
    
    _cellPost = cellPost;
    
    NSInteger topOffset = SGStartTopOffset;
    CGFloat heightLable = 0;
    
//  set Avatar&OwnerName&Date
    
    [self.avatarDateView.avatar setImageWithURL:cellPost.photoFromID];
    self.avatarDateView.dateCreation.text = cellPost.creationDate;
    self.avatarDateView.ownerName.text = cellPost.ownerName;
    
    topOffset += SGAvatarViewHeight + SGOffsetBetweenObjects_15;

//  set Label
    if (![cellPost.text isEqualToString:@""]) {
        
        heightLable = [self countHeightLable:cellPost.text];
        
        NSDictionary *attributes = [self attributesFont:17
                                              andShadow:(CGSizeMake(0, -1))
                                           andParagraph:NSTextAlignmentLeft];
        
        NSAttributedString *attribStr = [[NSAttributedString alloc]
                                         initWithString:cellPost.text
                                         attributes:attributes];
        
        UILabel *label = [[UILabel alloc]
                          initWithFrame:CGRectMake(SGContentOfContentViewHorisontalOffset / 2, topOffset,
                                                CGRectGetWidth(self.containerView.bounds) - SGContentOfContentViewHorisontalOffset, heightLable)];
        self.stringLabel = label;
        [label setAttributedText:attribStr];
        label.numberOfLines = 0;
        [self.containerView addSubview:label];
        
        topOffset += heightLable;
    }

//  set Images&Audios&Videos&Documents
    if (cellPost.postAttachment) {
    
        NSArray *arrayAttachmetTyeps = cellPost.postAttachment.attachmentTypesInOrder;
        NSArray *cleanedArrayAttachmetTyeps = [[NSSet setWithArray:arrayAttachmetTyeps ] allObjects];
        
        NSArray *arrayAttachmentPhoto = cellPost.postAttachment.arrayPhotoObj;
        NSArray *arrayAttachmentVideo = cellPost.postAttachment.arrayVideoObj;
        NSArray *arrayAttachmentMusic = cellPost.postAttachment.arrayAudioObj;
        NSArray *arrayAttachmentDoc = cellPost.postAttachment.arrayDocumentObj;
        
        for (NSNumber *number in cleanedArrayAttachmetTyeps) {
            
            NSUInteger typeAttachment = [number unsignedIntegerValue];
            
            switch (typeAttachment) {
                case SGAttachmentPhotos:
                    
                    topOffset += [self setImageWithHeightConteinerImageViewInRow:arrayAttachmentPhoto andTopOffset:topOffset];
                    break;
                case SGAttachmentVideo:
                    
                    topOffset += [self setImageWithHeightConteinerImageViewInRow:arrayAttachmentVideo andTopOffset:topOffset];
                    break;
                case SGAttachmentAudio:
                    
                    for (SGAudioObject *audio in arrayAttachmentMusic) {
                        
                        CGFloat xBoundsImages = SGContentOfContentViewHorisontalOffset / 2;
                        UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
                        if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
                            CGFloat widthForBounds = [UIScreen mainScreen].bounds.size.height;
                            xBoundsImages = (self.boundsForContentOfContentView - widthForBounds) / 2;
                        } else {
                            xBoundsImages = SGContentOfContentViewHorisontalOffset / 2;
                        }
                        
                        SGMusicConteiner *musicCont = [[[NSBundle mainBundle] loadNibNamed:@"MusicConteiner" owner:self options:nil] firstObject];
                        musicCont.autoresizingMask = UIViewAutoresizingNone;
                        
                        CGRect rectMusicConteiner = CGRectMake(xBoundsImages, topOffset, self.widthScreenOfDevice, SGMusicConteierHeight);
                        musicCont.frame = rectMusicConteiner;
                        
                        musicCont.artistLabel.text = audio.artist;
                        musicCont.titleLabel.text = audio.title;
                        musicCont.durationLabel.text = [SGServerObject timeFormatConvertToSeconds:audio.duration];
                        
                        [self.containerView addSubview:musicCont];
                        [self.imageViews addObject:musicCont];
                        
                        topOffset += SGMusicConteierHeight;
                    }
                    break;
                case SGAttachmentDoc:
                    
                    topOffset += [self setImageWithHeightConteinerImageViewInRow:arrayAttachmentDoc andTopOffset:topOffset];
                    break;
                case SGAttachmentUndefined:
                    
//                    NSLog(@"Do not find right identifier");
                    break;
                    
                default:
                    break;
            }
        }
    }
    
//  set Like&Repost
    self.likeRepostView.imageViewLike.highlighted = [cellPost.userLikes boolValue];
    self.likeRepostView.imageViewRepost.highlighted = [cellPost.userReposted boolValue];
    
    if (![[cellPost.postCountReposts stringValue] isEqualToString:@"0"]) {
        
        self.likeRepostView.quantityReposts.text = [cellPost.postCountReposts stringValue];
    } else {
        self.likeRepostView.quantityReposts.text = @"";
    }
    
    if (![[cellPost.postCountLikes stringValue] isEqualToString:@"0"]) {
        
        self.likeRepostView.quantityLikes.text = [cellPost.postCountLikes stringValue];
    } else {
        self.likeRepostView.quantityLikes.text = @"";
    }
    
    if (![[cellPost.postCountComents stringValue] isEqualToString:@"0"]) {
        
        self.likeRepostView.quantityComments.text = [cellPost.postCountComents stringValue];
    } else {
        self.likeRepostView.quantityComments.text = @"";
    }
}

- (CGFloat)countHeightLable:(NSString *)text {
    
    CGFloat offSet = 5.f;
    
    NSDictionary *attributes = [self attributesFont:17
                                          andShadow:(CGSizeMake(0, -1))
                                       andParagraph:NSTextAlignmentLeft];
    
    CGFloat boundsOfTextLabel = self.boundsForContentOfContentView - offSet * 2;
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake(boundsOfTextLabel, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    return ceilf(CGRectGetHeight(rect)) + offSet * 2;
}

- (NSDictionary *)attributesFont:(CGFloat)fontSize andShadow:(CGSize)shadowSize andParagraph:(NSTextAlignment)alignment {
    
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowOffset = shadowSize;
    shadow.shadowBlurRadius = 0.5;
    
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.alignment = alignment;
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font,       NSFontAttributeName,
                                paragraph,  NSParagraphStyleAttributeName,
                                shadow,     NSShadowAttributeName, nil];
    
    return attributes;
}

- (CGFloat)setImageWithHeightConteinerImageViewInRow:(NSArray *)imageObj andTopOffset:(CGFloat)topOffset {
    
    CGFloat sumOptimalHeightPictures = 0;
    NSInteger countAllPicture = 0;
    NSArray *arrayAttachment = imageObj;
    
    CGFloat yPicture = topOffset;
    
    while (countAllPicture < [arrayAttachment count]) {
        
        CGFloat optimalHeightPictures = 0;
        NSInteger countPinctureInRow = 0;
        CGFloat sumAspectRatio = 0;
        
        if ([arrayAttachment count] < 7) {
            self.preferredHeightForImageInRow = SGPreferredHeightForImageInRow_250;
        } else {
            self.preferredHeightForImageInRow = SGPreferredHeightForImageInRow_120;
        }
        
        if ([[imageObj firstObject] isKindOfClass:[SGPhotoObject class]]) {
            
            CGFloat widthForBoundsImages = 0;
            CGFloat xBoundsImages = 0;
            UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
            if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
                
                widthForBoundsImages = [UIScreen mainScreen].bounds.size.height;
                xBoundsImages = (self.widthScreenOfDevice - widthForBoundsImages) / 2;
            } else {
                widthForBoundsImages = self.boundsForContentOfContentView + 5;
                xBoundsImages = SGContentOfContentViewHorisontalOffset / 2;
            }
            
            do {
                SGPhotoObject *photoObj = [arrayAttachment objectAtIndex:countAllPicture];
                
                sumAspectRatio += [photoObj.width floatValue] / [photoObj.height floatValue];
                optimalHeightPictures = ceilf(widthForBoundsImages / sumAspectRatio);
                
                ++ countPinctureInRow;
                ++ countAllPicture;
            } while (countAllPicture < [arrayAttachment count] && optimalHeightPictures > self.preferredHeightForImageInRow);
            
            CGFloat xPicture = 0;
            
            for (NSInteger i = countAllPicture - countPinctureInRow; i < countAllPicture; i ++) {
                
                SGPhotoObject *photoObj = [arrayAttachment objectAtIndex:i];
                
                CGFloat widthParticularViewInRow = ([photoObj.width floatValue] * optimalHeightPictures) / [photoObj.height floatValue];
                
                UIImageView *imageView = [[UIImageView alloc] initWithFrame: CGRectMake(xPicture + xBoundsImages, yPicture, widthParticularViewInRow - 5, optimalHeightPictures - 5)];
                
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                
                [imageView setImageWithURL:photoObj.srcBig];
                [self.containerView addSubview:imageView];
                [self.imageViews addObject:imageView];
                
                xPicture += widthParticularViewInRow;
                
                countPinctureInRow = 0;
            }
            
        } else if ([[imageObj firstObject] isKindOfClass:[SGVideoObject class]]) {
            
            do {
                sumAspectRatio += 320 / 240.f;
                optimalHeightPictures = ceilf(self.boundsForContentOfContentView / sumAspectRatio);
                
                ++ countPinctureInRow;
                ++ countAllPicture;
            } while (countAllPicture < [arrayAttachment count] && optimalHeightPictures > 100);
            
            CGFloat xPicture = SGContentOfContentViewHorisontalOffset / 2;
            CGFloat widthPreviewInRow = ceilf(320 * optimalHeightPictures / 240);
            
            for (NSInteger i = countAllPicture - countPinctureInRow; i < countAllPicture; i ++) {
                
                SGVideoObject *photoVideo = [arrayAttachment objectAtIndex:i];
                
                UIImageView *imageView = [[UIImageView alloc] initWithFrame: CGRectMake(xPicture, yPicture, widthPreviewInRow - 5, optimalHeightPictures - 5)];
                
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                
                [imageView setImageWithURL:photoVideo.imageBig];
                [self.containerView addSubview:imageView];
                [self.imageViews addObject:imageView];
                
                xPicture += widthPreviewInRow;
                
                countPinctureInRow = 0;
            }
            
        } else if ([[imageObj firstObject] isKindOfClass:[SGDocumentObject class]]) {
            
            SGDocumentObject *doc = [imageObj objectAtIndex:countAllPicture];
            
            if ([doc.extension isEqualToString:@"gif"]) {
                
                do {
                    sumAspectRatio += 320 / 240.f;
                    optimalHeightPictures = ceilf(self.boundsForContentOfContentView / sumAspectRatio);
                    
                    ++ countPinctureInRow;
                    ++ countAllPicture;
                } while (countAllPicture < [arrayAttachment count] && optimalHeightPictures > 100);
                
                CGFloat xPicture = SGContentOfContentViewHorisontalOffset / 2;
                CGFloat widthPreviewInRow = ceilf(320 * optimalHeightPictures / 240);
                
                for (NSInteger i = countAllPicture - countPinctureInRow; i < countAllPicture; i ++) {
                    
                    UIImageView *imageView = [[UIImageView alloc] initWithFrame: CGRectMake(xPicture, yPicture, widthPreviewInRow - 5, optimalHeightPictures - 5)];
                    
                    imageView.contentMode = UIViewContentModeScaleAspectFit;
                    
                    [imageView setImageWithURL:doc.thumb];
                    [self.containerView addSubview:imageView];
                    [self.imageViews addObject:imageView];
                    
                    xPicture += widthPreviewInRow;
                    
                    countPinctureInRow = 0;
                }
                
            } else if ([doc.extension isEqualToString:@"pdf"]) {
                
                UIImage *imagePDF = [UIImage imageNamed:@"docImage.jpg"];
                UIImageView *ivPDF = [[UIImageView alloc] initWithFrame:
                                        CGRectMake(10, yPicture, SGDocumentIconHeight, SGDocumentIconHeight)];
                ivPDF.image = imagePDF;
                
                UILabel *lablePDF = [[UILabel alloc] initWithFrame:CGRectMake(SGDocumentIconHeight + SGContentOfContentViewHorisontalOffset / 2, yPicture + (SGDocumentIconHeight / 2), 300, 25)];
                lablePDF.text = @"PDF";
                
                [self.imageViews addObject:ivPDF];
                [self.imageViews addObject:lablePDF];
                [self.containerView addSubview:ivPDF];
                [self.containerView addSubview:lablePDF];
               
                
                yPicture += SGDocumentIconHeight;
                sumOptimalHeightPictures += SGDocumentIconHeight;
                ++ countAllPicture;
                
            } else {
                
                UIImage *imageError = [UIImage imageNamed:@"errorImage.png"];
                UIImageView *ivError = [[UIImageView alloc] initWithFrame:
                                        CGRectMake(SGContentOfContentViewHorisontalOffset / 2, yPicture, SGDocumentIconHeight, SGDocumentIconHeight)];
                ivError.image = imageError;
                [self.containerView addSubview:ivError];
                [self.imageViews addObject:ivError];
                
                yPicture += SGDocumentIconHeight;
                sumOptimalHeightPictures += SGDocumentIconHeight;
                ++ countAllPicture;
            }
        }
        
        yPicture += optimalHeightPictures;
        sumOptimalHeightPictures += optimalHeightPictures;
    }
    return sumOptimalHeightPictures;
}

- (CGFloat)heightConteinerImages:(NSArray *)imageObj {
    
    NSInteger countAllPicture = 0;
    NSArray *arrayAttachment = imageObj;
    CGFloat sumOptimalHeightPictures = 0;
    
    while (countAllPicture < [arrayAttachment count]) {
        
        CGFloat optimalHeightPictures = 0;
        CGFloat sumAspectRatio = 0;
        
        if ([arrayAttachment count] < 7) {
            self.preferredHeightForImageInRow = SGPreferredHeightForImageInRow_250;
        } else {
            self.preferredHeightForImageInRow = SGPreferredHeightForImageInRow_120;
        }
        
        if ([[imageObj firstObject] isKindOfClass:[SGPhotoObject class]]) {
            
            CGFloat widthForBoundsImages = 0;
            UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
            if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
                
                widthForBoundsImages = [UIScreen mainScreen].bounds.size.height;
            } else {
                widthForBoundsImages = self.boundsForContentOfContentView;
            }
            
            do {
                SGPhotoObject *photoObj = [arrayAttachment objectAtIndex:countAllPicture];
                
                sumAspectRatio += [photoObj.width floatValue] / [photoObj.height floatValue];
                optimalHeightPictures = widthForBoundsImages / sumAspectRatio;
                
                ++ countAllPicture;
            } while (countAllPicture < [arrayAttachment count] && optimalHeightPictures > self.preferredHeightForImageInRow);
            
        } else if ([[imageObj firstObject] isKindOfClass:[SGVideoObject class]]) {
            
            do {
                sumAspectRatio += 320 / 240.f;
                optimalHeightPictures = ceilf(self.boundsForContentOfContentView / sumAspectRatio);
                
                ++ countAllPicture;
            } while (countAllPicture < [arrayAttachment count] && optimalHeightPictures > 100);
            
        } else if ([[imageObj firstObject] isKindOfClass:[SGDocumentObject class]]) {
            
            SGDocumentObject *doc = [imageObj objectAtIndex:countAllPicture];
            
            if ([doc.extension isEqualToString:@"gif"]) {
                
                do {
                    sumAspectRatio += 320 / 240.f;
                    optimalHeightPictures = ceilf(self.boundsForContentOfContentView / sumAspectRatio);
                    
                    ++ countAllPicture;
                } while (countAllPicture < [arrayAttachment count] && optimalHeightPictures > 100);
                
            } else {
                sumOptimalHeightPictures += SGDocumentIconHeight;
                ++ countAllPicture;
            }
        }
        
        sumOptimalHeightPictures += optimalHeightPictures;
    }
    return sumOptimalHeightPictures;
}

- (void)setupCellMetricks {
    
    self.widthScreenOfDevice = [UIScreen mainScreen].bounds.size.width;
//    self.preferredHeightForImageInRow = ceilf(self.widthScreenOfDevice / 3.5);
    self.boundsForContentOfContentView = self.widthScreenOfDevice - SGConstraintHorizontalOffset - SGContentOfContentViewHorisontalOffset;
}

#pragma mark - CnfigureImageInRow

- (CGFloat) configureImageInRowByQuantityImages:(NSInteger)quantuty
                                          width:(CGFloat)widthImage
                                         height:(CGFloat)heightImage
                            preferredHeightLine:(CGFloat)heigtLine
                                       setImage:(NSURL *)urlImage
                                   andTopOffset:(CGFloat)topOffset {
    
    NSInteger countAllPicture = 0;
    CGFloat sumOptimalHeightPictures = 0;
    
    CGFloat optimalHeightPictures = 0;
    NSInteger countPinctureInRow = 0;
    CGFloat sumAspectRatio = 0;
    
     CGFloat yPicture = topOffset;
    
    do {
        sumAspectRatio += widthImage / heightImage;
        optimalHeightPictures = ceilf(self.widthScreenOfDevice / sumAspectRatio);
        
        ++ countPinctureInRow;
        ++ countAllPicture;
    } while (countAllPicture < quantuty && optimalHeightPictures > heigtLine);
    
    CGFloat xPicture = 0;
    
    for (NSInteger i = countAllPicture - countPinctureInRow; i < countAllPicture; i ++) {
        
        
        CGFloat widthParticularViewInRow = (widthImage * optimalHeightPictures) / heightImage;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame: CGRectMake(xPicture, yPicture, widthParticularViewInRow - 5, optimalHeightPictures - 5)];
        
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [imageView setImageWithURL:urlImage];
        [self.contentView addSubview:imageView];
        [self.imageViews addObject:imageView];
        
        xPicture += widthParticularViewInRow;
        
        countPinctureInRow = 0;
    }
    return sumOptimalHeightPictures;
}

#pragma mark - NSLayoutConstraint

- (void)addConstraintFromView:(UIView *)fView toView:(UIView *)sView leftSpace:(CGFloat)leading rightSpace:(CGFloat)trailing topSpace:(CGFloat)top bottomSpace:(CGFloat)bottom {
    
    
    NSLayoutConstraint *leftXConstraint = [NSLayoutConstraint
                                           constraintWithItem:fView attribute:NSLayoutAttributeLeft
                                           relatedBy:NSLayoutRelationEqual toItem:sView attribute:
                                           NSLayoutAttributeLeft multiplier:1.0f constant:leading];
    
    NSLayoutConstraint *rightXConstraint = [NSLayoutConstraint
                                            constraintWithItem:sView attribute:NSLayoutAttributeRight
                                            relatedBy:NSLayoutRelationEqual toItem:fView attribute:
                                            NSLayoutAttributeRight multiplier:1.0f constant:trailing];
    
    NSLayoutConstraint *TopYConstraint = [NSLayoutConstraint
                                          constraintWithItem:fView attribute:NSLayoutAttributeTop
                                          relatedBy:NSLayoutRelationEqual toItem:sView attribute:
                                          NSLayoutAttributeTop multiplier:1.0f constant:top];

    NSLayoutConstraint *bottomYConstraint = [NSLayoutConstraint
                                             constraintWithItem:sView attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual toItem:fView attribute:
                                             NSLayoutAttributeBottom multiplier:1.0f constant:bottom];

    [sView addConstraints:@[leftXConstraint, rightXConstraint, TopYConstraint, bottomYConstraint]];
}

- (void)likeRepostConstraints {
    
    self.likeRepostView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.likeRepostView
                                                            attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual
                                                               toItem:self.containerView
                                                            attribute:NSLayoutAttributeLeft
                                                           multiplier:1
                                                             constant:0];
    
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.containerView
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.likeRepostView
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.likeRepostView
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:SGLikeComentViewHeight];
    
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.likeRepostView
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.containerView
                                                             attribute:NSLayoutAttributeWidth
                                                            multiplier:1
                                                              constant:0];
    
    [self.containerView addConstraints:@[left, bottom, width]];
    [self.likeRepostView addConstraints:@[height]];
}

@end
