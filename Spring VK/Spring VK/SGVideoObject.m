//
//  SGVideoObject.m
//  45_APITest
//
//  Created by Sergey on 1/18/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGVideoObject.h"
#import "NSDictionary+SGUrlFromString.h"

@implementation SGVideoObject

- (id)initWithServerResponse:(NSDictionary *)responseObject {
    self = [super init];
    
    if (self) {
        
        self.date = [self createDateFromSec:[responseObject objectForKey:@"date"]];
        self.accessKey = [responseObject objectForKey:@"access_key"];
        self.ownerID = [responseObject objectForKey:@"owner_id"];
        self.title = [responseObject objectForKey:@"title"];
        self.descriptionVideo = [responseObject objectForKey:@"description"];
        self.duration = [responseObject objectForKey:@"duration"];
        self.videoID = [responseObject objectForKey:@"vid"];
        self.views = [responseObject objectForKey:@"views"];
        self.image = [responseObject makeUrlFromStringByKeyInDictionaty:@"image"];
        self.imageBig = [responseObject makeUrlFromStringByKeyInDictionaty:@"image_big"];
        self.imageSmall = [responseObject makeUrlFromStringByKeyInDictionaty:@"image_small"];
    }
    return self;
}

@end
