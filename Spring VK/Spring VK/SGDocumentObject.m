//
//  SGDocumentObject.m
//  45_APITest
//
//  Created by Sergey on 1/23/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGDocumentObject.h"
#import "NSDictionary+SGUrlFromString.h"


@implementation SGDocumentObject

- (id)initWithServerResponse:(NSDictionary *)responseObject {
   self = [super init];
    
    if (self) {
        
        self.date = [self createDateFromSec:[responseObject objectForKey:@"date"]];
        self.did = [responseObject objectForKey:@"did"];
        self.extension = [responseObject objectForKey:@"ext"];
        self.accessKey = [responseObject objectForKey:@"access_key"];
        self.ownerID = [responseObject objectForKey:@"owner_id"];
        self.title = [responseObject objectForKey:@"title"];
        self.size = [responseObject objectForKey:@"size"];
        self.thumb = [responseObject makeUrlFromStringByKeyInDictionaty:@"thumb"];
        self.thumbS = [responseObject makeUrlFromStringByKeyInDictionaty:@"thumb_s"];
        self.urlDoc = [responseObject objectForKey:@"url"];
    }
    return self;
}

@end
