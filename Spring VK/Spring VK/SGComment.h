//
//  SGComment.h
//  Spring VK
//
//  Created by Sergey on 2/23/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGServerObject.h"

#import "SGUser.h"

@interface SGComment : SGServerObject

@property (assign, nonatomic) BOOL     canEdit;
@property (strong, nonatomic) NSNumber *comUserID;
@property (strong, nonatomic) NSNumber *commentID;
@property (strong, nonatomic) NSString *commentDate;
@property (strong, nonatomic) NSString *commentText;

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSURL    *imageURLPhoto;

- (void)getUserData:(SGUser *)user;

@end

