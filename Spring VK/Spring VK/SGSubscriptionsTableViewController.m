//
//  SGSubscriptionsTableViewController.m
//  45_APITest
//
//  Created by Sergey on 1/6/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGSubscriptionsTableViewController.h"
#import "SGGroupWallViewController.h"

#import "SGServerManager.h"
#import "UIImageView+AFNetworking.h"

#import "SGCommunity.h"

@interface SGSubscriptionsTableViewController ()

@property (strong, nonatomic) NSMutableArray *arraySubscriptions;

@end

@implementation SGSubscriptionsTableViewController

static NSInteger numberSubsInRequest = 1000;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arraySubscriptions = [NSMutableArray new];
    
    [[SGServerManager sharedManager] getSubscriptionsUserID:self.currentID withOffset:0 andCount:numberSubsInRequest onSuccess:^(NSArray *subscriptions) {

        [self.arraySubscriptions setArray:subscriptions];
        [self.tableView reloadData];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.arraySubscriptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    id<SGSubscriptionProtocol> sub = [self.arraySubscriptions objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [sub name];
    NSURLRequest *request = [NSURLRequest requestWithURL:[sub avatarURL]];
    __weak UITableViewCell *weakCell = cell;
    [cell.imageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        
        weakCell.imageView.image = image;
        [weakCell setNeedsLayout];
        
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        
    }];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    SGCommunity *community = [self.arraySubscriptions objectAtIndex:indexPath.row];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SGGroupWallViewController *gvc = [storyboard instantiateViewControllerWithIdentifier:@"sg_wall"];
    
    NSString *groupID = [community.gID stringValue];
    
        if (![groupID hasPrefix:@"-"]) {
            groupID = [@"-" stringByAppendingString:groupID];
        }
    
    gvc.currentID = groupID;
    
    [self.navigationController pushViewController:gvc animated:YES];
}

@end
