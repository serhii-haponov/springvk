//
//  SGLikeRepost.m
//  45_APITest
//
//  Created by Sergey on 1/31/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGLikeRepost.h"

@implementation SGLikeRepost

- (IBAction)actionCommentButton:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(likeRepostComment:tagButton:)]) {
        
        [self.delegate likeRepostComment:self tagButton:self.commentButton.tag];
    }
}

@end
