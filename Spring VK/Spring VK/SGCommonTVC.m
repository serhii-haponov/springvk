//
//  SGCommonTVC.m
//  Spring VK
//
//  Created by Sergey on 4/20/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGCommonTVC.h"
#import "SWRevealViewController.h"

@interface SGCommonTVC ()

@end

@implementation SGCommonTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    if (self.loadFromSideMenu) {
        UIImage *image = [UIImage imageNamed:@"menu.png"];
        UIBarButtonItem *sandwichButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
        self.navigationItem.leftBarButtonItem = sandwichButton;
    }
}

@end
