//
//  SGServerObject.h
//  45_APITest
//
//  Created by Sergey on 12/27/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SGServerObject : NSObject

+ (SGServerObject *)factoryWithResponse:(NSDictionary *)responseObject;
+ (NSString *)timeFormatConvertToSeconds:(NSNumber *)timeSecs;

- (id)initWithServerResponse:(NSDictionary *)responseObject;
- (NSString *)createDateFromSec:(NSNumber *)second;

@end
