//
//  SGAudioObject.h
//  45_APITest
//
//  Created by Sergey on 1/20/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGServerObject.h"

@interface SGAudioObject : SGServerObject

@property (strong, nonatomic) NSNumber *audioID;
@property (strong, nonatomic) NSString *artist;
@property (strong, nonatomic) NSNumber *duration;
@property (strong, nonatomic) NSNumber *genre;
@property (strong, nonatomic) NSNumber *owner_ID;
@property (strong, nonatomic) NSString *performer;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *url;

@end
