//
//  SGMusicConteiner.h
//  45_APITest
//
//  Created by Sergey on 2/14/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SGMusicConteiner : UIView
@property (weak, nonatomic) IBOutlet UILabel *artistLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@end
