//
//  SGFriendCell.m
//  45_APITest
//
//  Created by Sergey on 1/9/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGFriendCell.h"
#import "SGUser.h"
#import "SGModifcation.h"
#import "UIImageView+AFNetworking.h"

@implementation SGFriendCell

-(void)awakeFromNib {
    [super awakeFromNib];
    
    UIImage *imagePlaceHolder = [UIImage imageNamed:@"person-placeholder.jpg"];
    imagePlaceHolder = [SGModifcation resizingPlaceHolder:imagePlaceHolder withSize:25];
    
    self.avatar.image = imagePlaceHolder;
    self.avatar.layer.cornerRadius = 13;
    self.avatar.clipsToBounds = YES;
    self.avatar.layer.shouldRasterize = YES;
    self.avatar.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    self.onlineBadge.layer.cornerRadius = 3;
}

- (void)setupWithUser:(SGUser *)user {
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
    
    if (user.online) {
        self.onlineBadge.backgroundColor = [UIColor greenColor];
    } else {
        self.onlineBadge.backgroundColor = [UIColor darkGrayColor];
    }

    NSURLRequest *request = [NSURLRequest requestWithURL:user.imageURL50];
    __weak SGFriendCell *weakCell = self;
    
    [self.avatar setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        weakCell.avatar.image = image;
        
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        
    }];
}

@end
