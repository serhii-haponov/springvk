//
//  ViewController.h
//  45_APITest
//
//  Created by Sergey on 11/16/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SGKindOfButton) {
    SGKindOfButtonNone = 0,
    SGKindOfButtonFriends,
    SGKindOfButtonFollowers,
    SGKindOfButtonSubscription,
    SGKindOfButtonWall
};

typedef NS_ENUM(NSUInteger, SGSortDescription) {
    SGSortDescriptionName = 0,
    SGSortDescriptionSurname,
    SGSortDescriptionOnline,
};

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmantedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSString *currentID;
@property (assign, nonatomic) BOOL loadFromSideMenu;
@property (assign, nonatomic) SGKindOfButton tagButton;

- (IBAction)changeSegmControl:(UISegmentedControl *)sender;

@end

