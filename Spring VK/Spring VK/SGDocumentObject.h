//
//  SGDocumentObject.h
//  45_APITest
//
//  Created by Sergey on 1/23/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGServerObject.h"

@interface SGDocumentObject : SGServerObject

@property (strong, nonatomic) NSString *date;

@property (strong, nonatomic) NSString *did;
@property (strong, nonatomic) NSString *extension;

@property (strong, nonatomic) NSString *accessKey;
@property (strong, nonatomic) NSString *ownerID;
@property (strong, nonatomic) NSString *title;

@property (strong, nonatomic) NSNumber *size;

@property (strong, nonatomic) NSURL    *thumb;
@property (strong, nonatomic) NSURL    *thumbS;
@property (strong, nonatomic) NSURL    *urlDoc;

@end
