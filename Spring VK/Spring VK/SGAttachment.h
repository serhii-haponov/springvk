//
//  SGAttachment.h
//  45_APITest
//
//  Created by Sergey on 1/14/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGServerObject.h"

@class SGPhotoObject;
@class SGAudioObject;
@class SGVideoObject;
@class SGDocumentObject;

typedef NS_ENUM(NSUInteger, SGAttachmentType) {
    SGAttachmentUndefined = 0,
    SGAttachmentPhotos,
    SGAttachmentVideo,
    SGAttachmentAudio,
    SGAttachmentDoc
};

@interface SGAttachment : SGServerObject

@property (assign, nonatomic) SGAttachmentType type;
@property (strong, nonatomic) NSMutableArray *attachmentTypesInOrder;

@property (strong, nonatomic) NSString *ownerID;

@property (strong, nonatomic) NSString *typeAttachment;
@property (strong, nonatomic) NSMutableArray<SGPhotoObject *>    *arrayPhotoObj;
@property (strong, nonatomic) NSMutableArray<SGAudioObject *>    *arrayAudioObj;
@property (strong, nonatomic) NSMutableArray<SGVideoObject *>    *arrayVideoObj;
@property (strong, nonatomic) NSMutableArray<SGDocumentObject *> *arrayDocumentObj;

@end
