//
//  SGProfileCell.m
//  Spring VK
//
//  Created by Sergey on 4/24/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGProfileCell.h"
#import "UIImageView+AFNetworking.h"
#import "SGUser.h"

#import "SGServerManager.h"

@implementation SGProfileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backView.layer.cornerRadius = CGRectGetWidth(self.backView.frame) / 2;
    self.userAvatar.layer.cornerRadius = CGRectGetWidth(self.userAvatar.frame) / 2;
    
    SGUser *user = [SGServerManager sharedManager].userInfo;
    
    NSURL *imageUrl = user.imageURL50;
    [self.userAvatar setImageWithURL:imageUrl];
    self.userName.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
