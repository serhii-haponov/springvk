//
//  NavController.m
//  45_APITest
//
//  Created by Sergey on 2/8/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "NavController.h"
#import "SGPhotoController.h"

@interface NavController () <UINavigationControllerDelegate>

@property (strong, nonatomic) UIView *statusBarView;

@end

@implementation NavController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
    
    UIApplication *app = [UIApplication sharedApplication];
    CGFloat statusBarHeight = app.statusBarFrame.size.height;
    
    UIView *statusBarView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, statusBarHeight)];
    statusBarView.backgroundColor  =  [UIColor colorWithWhite:0.9 alpha:1.0];
    [self.view addSubview:statusBarView];
    self.statusBarView  = statusBarView;
    [self.view bringSubviewToFront:self.navigationBar];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
            
            self.statusBarView.alpha = 0;
        } else {
            self.statusBarView.alpha = 1;
        }
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        
    }];
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if ([viewController isKindOfClass:[SGPhotoController class]]) {
        self.hidesBarsOnSwipe = NO;
    } else {
        self.hidesBarsOnSwipe = YES;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//-(BOOL)shouldAutorotate {
//    return [self.topViewController shouldAutorotate];
//}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [self.topViewController preferredInterfaceOrientationForPresentation];
}

- (UIInterfaceOrientationMask)navigationControllerSupportedInterfaceOrientations:(UINavigationController *)navigationController {
    return UIInterfaceOrientationMaskAll;
}

@end
