//
//  SGFollowerCell.m
//  45_APITest
//
//  Created by Sergey on 1/9/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGFollowerCell.h"
#import "UIKit+AFNetworking.h"

@implementation SGFollowerCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Image method

- (void)setImageByUrl:(NSURL *)url {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.thumbnailImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        
    }];
}

@end
