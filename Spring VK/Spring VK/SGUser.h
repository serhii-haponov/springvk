//
//  SGUser.h
//  45_APITest
//
//  Created by Sergey on 11/27/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "SGServerObject.h"
#import "SGSubscriptionProtocol.h"

@interface SGUser : SGServerObject <SGSubscriptionProtocol>

@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSURL *imageURL50;
@property (strong, nonatomic) NSURL *imageURLPhoto;
@property (strong, nonatomic) NSURL *imageURLBig;
@property (assign, nonatomic) BOOL online;

@end
