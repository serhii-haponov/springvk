//
//  NSDictionary+SGUrlFromString.m
//  45_APITest
//
//  Created by Sergey on 1/25/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "NSDictionary+SGUrlFromString.h"

@implementation NSDictionary (SGUrlFromString)

- (NSURL *)makeUrlFromStringByKeyInDictionaty:(NSString *)key {
    
    NSString *urlString = [self objectForKey:key];
    NSURL *url = [NSURL URLWithString:urlString];
    
    return url;
}

@end
