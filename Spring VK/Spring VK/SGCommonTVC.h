//
//  SGCommonTVC.h
//  Spring VK
//
//  Created by Sergey on 4/20/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SGUser.h"

@interface SGCommonTVC : UITableViewController

@property (strong, nonatomic) NSString *currentID;
@property (assign, nonatomic) BOOL loadFromSideMenu;

@end
