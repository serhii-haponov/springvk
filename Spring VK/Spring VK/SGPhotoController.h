//
//  SGPhotoController.h
//  45_APITest
//
//  Created by Sergey on 12/9/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SGUser;

@interface SGPhotoController : UIViewController

@property (strong, nonatomic) SGUser *person;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *photoUser;
@property (weak, nonatomic) IBOutlet UILabel *nameUser;
@property (weak, nonatomic) IBOutlet UILabel *familyUser;

@end
