//
//  SGAttachment.m
//  45_APITest
//
//  Created by Sergey on 1/14/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

#import "SGAttachment.h"
#import "SGPhotoObject.h"
#import "SGAudioObject.h"
#import "SGVideoObject.h"
#import "SGDocumentObject.h"

@implementation SGAttachment

- (id)initWithServerResponse:(NSDictionary *)responseObject {

    self = [super init];
    
    if (self) {
        
        self.arrayPhotoObj = [[NSMutableArray alloc] init];
        self.arrayAudioObj = [[NSMutableArray alloc] init];
        self.arrayVideoObj = [[NSMutableArray alloc] init];
        self.arrayDocumentObj = [[NSMutableArray alloc] init];
        self.attachmentTypesInOrder = [[NSMutableArray alloc] init];
        
        NSArray *arrayAttachments = [responseObject objectForKey:@"attachments"];
        
        for (NSDictionary *dictAttachmen in arrayAttachments) {
            
            self.typeAttachment = [dictAttachmen objectForKey:@"type"];
            
            if ([self.typeAttachment isEqualToString:@"photo"]) {
                
                NSDictionary *dictPhotoParams = [dictAttachmen objectForKey:@"photo"];
                SGPhotoObject *photo = [[SGPhotoObject alloc] initWithServerResponse:dictPhotoParams];
                [self.arrayPhotoObj addObject:photo];
                
                self.type = SGAttachmentPhotos;
                [self.attachmentTypesInOrder addObject:@(self.type)];
                
            } else if ([self.typeAttachment isEqualToString:@"audio"]) {
                
                NSDictionary *dictPhotoParams = [dictAttachmen objectForKey:@"audio"];
                SGAudioObject *audio = [[SGAudioObject alloc] initWithServerResponse:dictPhotoParams];
                [self.arrayAudioObj addObject:audio];
                
                self.type = SGAttachmentAudio;
                [self.attachmentTypesInOrder addObject:@(self.type)];
                
            } else if ([self.typeAttachment isEqualToString:@"video"]) {
                
                NSDictionary *dictPhotoParams = [dictAttachmen objectForKey:@"video"];
                SGVideoObject *video = [[SGVideoObject alloc] initWithServerResponse:dictPhotoParams];
                [self.arrayVideoObj addObject:video];
                
                self.type = SGAttachmentVideo;
                [self.attachmentTypesInOrder addObject:@(self.type)];
                
            } else if ([self.typeAttachment isEqualToString:@"doc"]) {
                
                NSDictionary *dictPhotoParams = [dictAttachmen objectForKey:@"doc"];
                SGDocumentObject *doc = [[SGDocumentObject alloc] initWithServerResponse:dictPhotoParams];
                [self.arrayDocumentObj addObject:doc];
                
                self.type = SGAttachmentDoc;
                [self.attachmentTypesInOrder addObject:@(self.type)];
                
            } else {
                self.type = SGAttachmentUndefined;
                [self.attachmentTypesInOrder addObject:@(self.type)];
            }
        }
    }
    return self;
}

@end
