//
//  SGServerManager.m
//  45_APITest
//
//  Created by Sergey on 11/16/16.
//  Copyright © 2016 Gaponov. All rights reserved.
//

#import "SGServerManager.h"
#import "AFNetworking.h"
#import "SGLoginViewController.h"

#import "SGCommunity.h"

@interface SGServerManager ()

@property (strong, nonatomic) AFHTTPSessionManager *requestSessionMamager;

@end

static NSString *kUserToken = @"token";
static NSString *kUserID    = @"userID";

@implementation SGServerManager

+ (SGServerManager *)sharedManager {
    
    static SGServerManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SGServerManager alloc] init];
    });
    
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSURL *url = [NSURL URLWithString:@"https://api.vk.com/method/"];
        self.requestSessionMamager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    }
    return self;
}

- (void)authorizeUser:(void(^)(SGUser *user))completion {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *tokenUser = [defaults objectForKey:kUserToken];
    NSString *userID = [defaults objectForKey:kUserID];

    if (tokenUser) {
        
        SGAccessToken *token = [[SGAccessToken alloc] init];
        
        token.token = tokenUser;
        token.userId = userID;
        
        self.accessToken = token;
        
        [self getUser:userID onSuccess:^(SGUser *user) {
            
            if (completion) {
                completion(user);
            }
        } onFailure:^(NSError *error, NSInteger statuseCode) {
            if (completion) {
                completion(nil);
            }
        }];
        
    } else {
        
        SGLoginViewController *vc = [[SGLoginViewController alloc] initWithComplationBlock:^(SGAccessToken *token) {
            self.accessToken = token;
            
            if (token) {
                
                [defaults setObject:token.token forKey:kUserToken];
                [defaults setObject:token.userId forKey:kUserID];
                [defaults synchronize];
                
                [self getUser: self.accessToken.userId onSuccess:^(SGUser *user) {
                    
                    if (completion) {
                        completion(user);
                    }
                } onFailure:^(NSError *error, NSInteger statuseCode) {
                    if (completion) {
                        completion(nil);
                    }
                }];
                
            } else  if (completion) {
                completion(nil);
            }
        }];
        
        UINavigationController *nv = [[UINavigationController alloc] initWithRootViewController:vc];
        
        UIViewController *mainVC = [[[UIApplication sharedApplication] windows] firstObject].rootViewController;
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [mainVC presentViewController:nv animated:YES completion:nil];
        });
    }
}

- (void)getUser:(NSString *)userId
      onSuccess:(void(^)(SGUser *user))success
      onFailure:(void(^)(NSError *error, NSInteger statuseCode))failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            userId,       @"user_ids",
                            @"photo_big,\
                            photo_50,\
                            photo",       @"fields",
                            @"nom",       @"name_case",nil];
    
    [self.requestSessionMamager GET:@"users.get"
                         parameters:params progress:nil
                            success:^(NSURLSessionTask *operation, NSDictionary *responseObject) {
                                
                                NSArray *dictArray = [responseObject objectForKey:@"response"];
                                
                                if ([dictArray count] > 0) {
                                    self.userInfo = [[SGUser alloc] initWithServerResponse:[dictArray firstObject]];
                                    if (success) {
                                        success(self.userInfo);
                                    }
                                } else {
                                    if (failure) {
                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)operation.response;
                                        failure(nil, httpResponse.statusCode);
                                    }
                                }
                            } failure:^(NSURLSessionTask *operation, NSError *error) {
                                NSLog(@"Error: %@", error);
                                
                                if (failure) {
                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)operation.response;
                                    failure(error, httpResponse.statusCode);
                                }
                            }];
}

- (void)getFriendsWithUserID:(NSString *)userId
                      Offset:(NSInteger)offSet
                       count:(NSInteger)count
                   onSuccess:(void(^)(NSArray *friends))success
                   onFailure:(void(^)(NSError *error, NSInteger statuseCode))failure {

    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.accessToken.token,    @"access_token",
                            userId,      @"user_id",
                            @"name",      @"order",
                            @(count),     @"count",
                            @(offSet),    @"offset",
                            @"photo_50,\
                            photo_big,\
                            online",      @"fields",
                            @"nom",       @"name_case",nil];
    
    [self.requestSessionMamager GET:@"friends.get"
                         parameters:params progress:nil
                            success:^(NSURLSessionTask *task, NSDictionary *responseObject) {
                                
        NSArray *dictArray = [responseObject objectForKey:@"response"];
        NSMutableArray *objectsArray = [NSMutableArray new];
        
        for (NSDictionary *dict in dictArray) {
            self.usersFriends = [[SGUser alloc] initWithServerResponse:dict];
            [objectsArray addObject:self.usersFriends];
        }
        if (success) {
            success(objectsArray);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        if (failure) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)operation.response;
            failure(error, httpResponse.statusCode);
        }
    }];
}

// -136226205 id group
//  39229810 my id

- (NSURLSessionDataTask *)getGroupWall:(NSString *)groupId
          withOffSet:(NSInteger)offSet
            andCount:(NSInteger)count
           onSuccess:(void(^)(NSArray *post))success
           onFailure:(void(^)(NSError *error, NSInteger statuseCode)) failure {
    
    if ([groupId isKindOfClass:[SGCommunity class]] && ![groupId hasPrefix:@"-"]) {
        groupId = [@"-" stringByAppendingString:groupId];
    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.accessToken.token,    @"access_token",
                            groupId,                   @"owner_id",
                            @(offSet),                 @"offset",
                            @(count),                  @"count",
                            @"all",                    @"filter",
                            @(1),                      @"extended",
                            nil];
    
   NSURLSessionDataTask *task = [self.requestSessionMamager GET:@"wall.get"
                         parameters:params progress:nil
                            success:^(NSURLSessionTask *operation, NSDictionary *responseObject) {
                                
                                NSDictionary *dictExtended = [responseObject objectForKey:@"response"];
                                NSArray *postsArray = [dictExtended objectForKey:@"wall"];
                                self.quantityPosts = [[postsArray firstObject] integerValue];
                                if ([postsArray count] > 1) {
                                    postsArray = [postsArray subarrayWithRange:NSMakeRange(1, (int)[postsArray count] - 1)];
                                } else {
                                    postsArray = nil;
                                }
                                
                                NSMutableArray *objectsArray = [NSMutableArray new];
                                for (NSDictionary *dict in postsArray) {
                                     SGPost *post = [[SGPost alloc] initWithServerResponse:dict];
                                    [post receavePhotoUrlFromID:dictExtended];
                                    [objectsArray addObject:post];
                                }
                                
                                    if (success) {
                                        success(objectsArray);
                                } else if (failure) {
                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)operation.response;
                                        failure(nil, httpResponse.statusCode);
                                    }
                            } failure:^(NSURLSessionTask *operation, NSError *error) {
                                NSLog(@"Error: %@", error);
                                
                                if (failure) {
                                    
                                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)operation.response;
                                    failure(error, httpResponse.statusCode);
                                }
                            }];
    return task;
}

- (NSURLSessionDataTask *)getOwnersWallComments:(NSString *)ownerID
                    ownerPost:(NSString *)postID
                    withCount:(NSInteger)count
                    andOffset:(NSInteger)offset
                    inSuccess:(void(^)(NSArray *comments))success
                    inFailure:(void(^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            ownerID,   @"owner_id",
                            postID,    @"post_id",
                            @(count),  @"count",
                            @(offset), @"offset",
//                            @"desc",   @"sort",
//                            @(0),      @"extended",
                            self.accessToken.token,    @"access_token", nil];
    
    
  NSURLSessionDataTask *task = [self.requestSessionMamager GET:@"wall.getComments" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
   
      NSArray *arrayCommentsElements = [responseObject objectForKey:@"response"];
      arrayCommentsElements = [arrayCommentsElements subarrayWithRange:NSMakeRange(1, [arrayCommentsElements count] - 1)];
      
      NSMutableArray *arrayCommentsObj = [NSMutableArray new];
      for (NSDictionary *dicComment in arrayCommentsElements) {
          
          SGComment *comObj = [[SGComment alloc] initWithServerResponse:dicComment];
          [arrayCommentsObj addObject:comObj];
      }
      
          if (success) {
              success(arrayCommentsObj);
          } else {
              NSLog(@"Do not initial success block");
          }
      
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"ERROR %@", error);
        if (failure) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
            failure(error, httpResponse.statusCode);
        }
    }];
    return task;
}


- (void)postText:(NSString *)text
        forGroup:(NSString *)groupID
       inSuccess:(void(^)(id result))success
        inFailure:(void(^)(NSError *error, NSInteger statusCode))failure {
    
//    if (![groupID hasPrefix:@"-"]) {
//        groupID = [@"-" stringByAppendingString:groupID];
//    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            groupID,                   @"owner_id",
                            text,                      @"message",
                            self.accessToken.token,    @"access_token", nil];
    
    [self.requestSessionMamager POST:@"wall.post" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
          NSLog(@"ERROR %@", error);
        
        if (failure) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
            failure(error, httpResponse.statusCode);
        }
    }];
}

- (void)createComment:(NSString *)text
        inWallID:(NSString *)groupID
       forPostID:(NSString *)postID
       inSuccess:(void(^)(id result))success
       inFailure:(void(^)(NSError *error, NSInteger statusCode))failure {
    
    if (![groupID hasPrefix:@"-"]) {
        groupID = [@"-" stringByAppendingString:groupID];
    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            groupID,                   @"owner_id",
                            postID,                    @"post_id",
                            text,                      @"message",
                            self.accessToken.token,    @"access_token", nil];
    
    [self.requestSessionMamager POST:@"wall.createComment" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"ERROR %@", error);
        
        if (failure) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
            failure(error, httpResponse.statusCode);
        }
    }];
}

- (void)getVideosByIds:(NSArray *)vids ownersIds:(NSArray *)ownerIds
              forGroup:(NSString *)groupID
             inSuccess:(void(^)(id result))success
             inFailure:(void(^)(NSError *error, NSInteger statusCode))failure; {
    
    NSString *vid = vids[0];
    NSString *owner_id = ownerIds[0];
    
    NSString *videoInfo = [NSString stringWithFormat:@"%@_%@", owner_id, vid];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            groupID,                   @"owner_id",
                            videoInfo,                 @"videos",
                            @(1),                      @"extended",
                            self.accessToken.token,    @"access_token", nil];
    
    [self.requestSessionMamager POST:@"video.get" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"ERROR %@", error);
        
        if (failure) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
            failure(error, httpResponse.statusCode);
        }
    }];
}


- (void)getSubscriptionsUserID:(NSString *)userID
                    withOffset:(NSInteger)offset
                      andCount:(NSInteger)count
                     onSuccess:(void(^)(NSArray *subscriptions))success
                     onFailure:(void(^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *parameters = @{@"access_token" : self.accessToken.token,
                                 @"user_id"      : userID,
                                 @"offset"       : @(offset),
                                 @"count"        : @(count),
                                 @"extended"     : @(1),
                                 @"fields"       : @"photo_100"};
    
    [self.requestSessionMamager GET:@"users.getSubscriptions" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        NSArray *arrayAllSubs = [responseObject objectForKey:@"response"];
        
        NSMutableArray *arrayParsSubs = [NSMutableArray new];
        
        for (NSDictionary *dict in arrayAllSubs) {
            SGServerObject *object = [SGServerObject factoryWithResponse: dict];
            [arrayParsSubs addObject: object];
        }
        if (success) {
            success(arrayParsSubs);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
            failure(error, httpResponse.statusCode);
        }
    }];
}

- (void)getFollowersUserID:(NSString *)userID
                    withOffset:(NSInteger)offset
                      andCount:(NSInteger)count
                     onSuccess:(void(^)(NSArray *followers))success
                     onFailure:(void(^)(NSError *error, NSInteger statusCode))failure {
    
    NSDictionary *parameters = @{@"access_token" : self.accessToken.token,
                                 @"user_id"      : userID,
                                 @"offset"       : @(offset),
                                 @"count"        : @(count),
                                 @"extended"     : @(1),
                                 @"fields"       : @"photo_50, photo_big, online"};
    
    [self.requestSessionMamager GET:@"users.getFollowers" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dictResponseObj = [responseObject objectForKey:@"response"];
        NSArray *dictFollowers = [dictResponseObj objectForKey:@"items"];
        
        NSMutableArray *arrayFollowers = [NSMutableArray new];
        for (NSDictionary *dict in dictFollowers) {
            SGUser *follower = [[SGUser alloc] initWithServerResponse:dict];
            [arrayFollowers addObject:follower];
        }
        if (success) {
            success(arrayFollowers);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        failure(error, httpResponse.statusCode);
    }];
}
@end

















